#%%
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from colour import Color
import sys

arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

df_ea = pd.read_table(getArg(0, 'results-ea-distances.csv'), sep=";", header=None)
df_ea.iloc[:,0] = df_ea.iloc[:,0].apply(lambda x: float(x))
#%%
pca = PCA(n_components=2)
X2d = pca.fit_transform(df_ea.iloc[:,1:].values)
# %%
print(str(float(df_ea[df_ea.iloc[:,0] > 0].iloc[:,0].min())))
def calcColor(fitness, colors):
  minFitness = float(df_ea[df_ea.iloc[:,0] > 0].iloc[:,0].min())
  ratio = minFitness / fitness
  index = min(int(round(len(colors) * ratio)), len(colors) - 1)
  pointColor = colors[index].rgb
  if(fitness == minFitness):
    pointColor = Color("red").rgb
  return (*pointColor, ratio)

colors = list(Color("yellow").range_to(Color("darkblue"), 500))
df = pd.DataFrame(data=X2d, columns=["x", "y"])
df_ea.iloc[:,0] = df_ea.iloc[:,0].apply(lambda x: float(x))
#df_ea['Alpha'] = minFitness / df_ea.iloc[:,0]
df['Fitness'] = df_ea.iloc[:,0]
df['Color'] = df_ea.iloc[:,0].apply(lambda x: calcColor(x, colors))
#%%
fig=plt.figure()
fig = plt.figure(figsize=(5,5))
plt.gca().set_aspect('equal', adjustable='box')
plt.scatter(df['x'], df['y'], c=df['Color'], edgecolors="none")
best = df.loc[df['Fitness'].idxmin()]
plt.scatter(best['x'], best['y'], c=best['Color'], edgecolors="none")
#plt.gca().set_xticklabels([])
#plt.gca().set_yticklabels([])
#plt.gca().set_xticks([])
#plt.gca().set_yticks([])
plt.savefig(getArg(1, './distances.pdf'), bbox_inches='tight')
#plt.show()

# %%
