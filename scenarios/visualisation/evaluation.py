'''
* Brute Force Box Plot
* Combined box plot of all configs
* Combined box plot of all configs & brute force
'''

# %%
print('Importing...')

import pandas as pd
import math
import matplotlib.pyplot as plt
import sys

arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

#%%
def getIdent(config):
  print(config)
  if 'EX-FC-LS' in config:
    return 'Fast Convergence with Exploration\n& Local Search - FC-EX-LS'
  elif 'PED-EX-FC-ME-RT' in config:
    return 'Fast Convergence with Exploration\nRe-Evaluation\nPedestrians\nReduced-Time'
  elif 'PED-EX-FC-ME' in config:
    return 'Fast Convergence with Exploration\nRe-Evaluation\nPedestrians'
  elif 'EX-FC-ME' in config:
    return 'Fast Convergence with Exploration\nRe-Evaluation'
  elif 'EX-FC' in config:
    return 'Fast Convergence with\nExploration - FC-EX'
  elif 'PED-FC-ME-RT' in config:
    return 'Fast Convergence\nRe-Evaluation\nPedestrians\nReduced-Time'
  elif 'PED-FC-ME' in config:
    return 'Fast Convergence\nRe-Evaluation\nPedestrians'
  elif 'FC-ME' in config:
    return 'Fast Convergence\nRe-Evaluation'
  elif 'FC' in config:
    return 'Fast Convergence - FC'
  elif 'PSOC' in config:
    return 'Patricle Swarm Optimisation\n(Calm) - PSOC'
  elif 'PSOA' in config:
    return 'Patricle Swarm Optimisation\n(Active) - PSOA'
  elif 'FIXED' in config:
    return 'Fixed Plan'
  elif 'ADAPTIVE' in config:
    return 'Adaptive Plan'
  elif 'RN' in config:
    return 'Random Executions'  
  elif 'BF' in config:
    return 'Brute-Force Execution'

#%%
def createBruteForceOnly():
  print('Creating only brute force box plot...')
  
  print('Importing Brute Force Values...')
  df = pd.read_table('./' + getArg(3, 'C-EWS-BF') + '/results-' + getArg(3, 'C-EWS-BF') + '.csv', sep=";")
  df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
  df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
  df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))

  uniques = len(df.drop_duplicates())
  print("Duplicate values: " + str(df.shape[0] - uniques))
  df = df.drop_duplicates()

  df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]
  print("Valid values: " + str(df_validFitness.shape[0]))
  print("Invalid values: " + str(df.shape[0] - df_validFitness.shape[0]))
  print("Min: " + str(df_validFitness['Fitness'].min()))
  print("Max: " + str(df_validFitness['Fitness'].max()))

  print('Creating plot...')
  fig = plt.figure(figsize=(10,1))
  ax=fig.add_axes([0,0,1,1])
  ax.set_title(getArg(0, ''), fontdict={'fontsize': 12}, pad=20)
  bp_dict = ax.boxplot(df_validFitness['Fitness'], vert=False, showmeans=True)
  ax.set_xlabel('Fitness')
  ax.set_ylabel('')
  ax.set_yticklabels(['Brute-Force\nn: ' + str(int(round(df_validFitness.shape[0])))])

  for index,line in enumerate(bp_dict['medians']):
    x, y = line.get_xydata()[1]
    ax.annotate('%.1f' % x, xy=(x, y + 0.1))

  for index,line in enumerate(bp_dict['means']):
      x, y = line.get_xydata()[0]
      ax.annotate('%.1f' % x, xy=(x, y - .3))

  for index,line in enumerate(bp_dict['caps']):
      movement = -0.06
      if(index % 2 > 0):
        movement = 0.04
      x, y = line.get_xydata()[0]
      ax.annotate('%.1f' % x, xy=(x + (x * movement), y - .3))

  for index,line in enumerate(bp_dict['whiskers']):
      x, y = line.get_xydata()[0]
      ax.annotate('%.1f' % x, xy=(x, y - .3))

  print('Saving plot...')
  plt.savefig('./' + getArg(0, '') + '-brute-force-final.pdf', bbox_inches='tight')
  print('Done.')

#%%
def createAlgorithmOnly():
  print('Creating only algorithm plot...')

  dfs = []
  yTicks = []
  for i in range(len(arguments) - 2, 3, -1):
    config = getArg(i, '')
    yTicks.append(getIdent(config))
    print('Importing ' + config + ' values...')
    df = pd.read_table('./' + config + '/results-ea-batch-' + config + '-results.csv', sep=";")
    df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
    df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
    df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))

    df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]

    # avg. chromosomes
    #print('Avg Chromosomes evaluated: %.2f' % df['Chromosomes'].mean())

    # avg. Fitness
    print('Avg Fitness: %.2f' % df_validFitness['Fitness'].mean())
    print('Max Fitness: %.2f' % df_validFitness['Fitness'].max())
    print('Min Fitness: %.2f' % df_validFitness['Fitness'].min())
    #print('Avg. Generation: %.2f' % df_validFitness['GenerationFound'].mean())
    print('Chromosomes with valid fitness: ' + str(df_validFitness.shape[0]))
    print('Invalid Fitness: ' + str(df['Fitness'].shape[0] - df_validFitness.shape[0]))

    dfs.append(df_validFitness['Fitness'])

  fig = plt.figure(figsize=(20,6))
  ax=fig.subplots();
  ax.set_title(getArg(0, ''), fontdict={'fontsize': 12}, pad=20)
  ax.set_xlabel('Fitness')
  ax.set_ylabel('')
  bp_dict = ax.boxplot(dfs, vert=False, showmeans=True, widths=0.1)
  for index,line in enumerate(bp_dict['medians']):
      x, y = line.get_xydata()[1]
      ax.annotate('%.1f' % x, xy=(x - 0.1, y + 0.2))
  '''
  for index,line in enumerate(bp_dict['means']):
      x, y = line.get_xydata()[0]
      ax.annotate('%.1f' % x, xy=(x, y - .3))

  for index,line in enumerate(bp_dict['caps']):
      x, y = line.get_xydata()[0]
      ax.annotate('%.1f' % x, xy=(x, y - .3))
      '''
  for index,line in enumerate(bp_dict['whiskers']):
      x, y = line.get_xydata()[0]
      ax.annotate('%.1f' % x, xy=(x, y - .3))
  
  ax.set_yticklabels(yTicks)
  #ax.invert_yaxis()
  if getArg(1, 0.0) != '-':
    plt.axvline(x=float(getArg(1, 220.3)), c="#4A4A4A", linestyle="--")
    plt.text(float(getArg(1, 220.3)) + 2, 0.7, "Adaptive Plan", dict(size=8, c='#000000'))
    plt.axvline(x=float(getArg(2, 220.3)), c="#4A4A4A", linestyle="--")
    plt.text(float(getArg(2, 220.3)) + 2, 0.7, "Fixed Plan", dict(size=8, c='#000000'))
  plt.savefig(getArg(0, '') + '-algorithms-final.pdf', bbox_inches='tight')


#%%
def createCombined():
  print('Creating combined plot...')

  print('Importing Brute Force Values...')
  df = pd.read_table('./' + getArg(3, 'C-EWS-BF') + '/results-' + getArg(3, 'C-EWS-BF') + '.csv', sep=";")
  df['Fitness'] = df['Time-All']
  df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
  df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
  df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))

  uniques = len(df.drop_duplicates())
  print("Duplicate values: " + str(df.shape[0] - uniques))
  df = df.drop_duplicates()

  df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]
  print("Valid values: " + str(df_validFitness.shape[0]))
  print("Invalid values: " + str(df.shape[0] - df_validFitness.shape[0]))

  dfs = [df_validFitness['Fitness']]
  yTicks = ['Brute-Force (~ 2 Million random plans)']
  for i in range(len(arguments) - 2, 3, -1):
    config = getArg(i, '')
    yTicks.append(getIdent(config))
    print('Importing ' + config + ' values...')
    df = pd.read_table('./' + config + '/results-ea-batch-' + config + '-results.csv', sep=";")
    df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
    df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
    df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))

    df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]

    # avg. chromosomes
    print('Avg Chromosomes evaluated: %.2f' % df['Chromosomes'].mean())

    # avg. Fitness
    print('Avg Fitness: %.2f' % df_validFitness['Fitness'].mean())
    print('Max Fitness: %.2f' % df_validFitness['Fitness'].max())
    print('Min Fitness: %.2f' % df_validFitness['Fitness'].min())
    print('Avg. Generation: %.2f' % df_validFitness['GenerationFound'].mean())
    print('Chromosomes with valid fitness: ' + str(df_validFitness.shape[0]))
    print('Invalid Fitness: ' + str(df['Fitness'].shape[0] - df_validFitness.shape[0]))

    dfs.append(df_validFitness['Fitness'])

  fig = plt.figure(figsize=(20,7))
  ax=fig.subplots();
  ax.set_title(getArg(0, ''), fontdict={'fontsize': 12}, pad=20)
  ax.set_xlabel('Fitness')
  ax.set_ylabel('')
  bp_dict = ax.boxplot(dfs, vert=False, showmeans=True, widths=0.1)
  for index,line in enumerate(bp_dict['medians']):
      x, y = line.get_xydata()[1]
      ax.annotate('%.1f' % x, xy=(x - 0.1, y + 0.2))
  
  ax.set_yticklabels(yTicks)
  if getArg(1, 0.0) != '-':
    plt.axvline(x=float(getArg(1, 0.0)), c="#4A4A4A", linestyle="--")
    plt.text(float(getArg(1, 0.0)) + 2, 0.7, "Adaptive Plan", dict(size=8, c='#000000'))
    plt.axvline(x=float(getArg(2, 0.0)), c="#4A4A4A", linestyle="--")
    plt.text(float(getArg(2, 0.0)) + 2, 0.7, "Fixed Plan", dict(size=8, c='#000000'))
  plt.savefig(getArg(0, '') + '-algorithms-brute-force-final.pdf', bbox_inches='tight')

# %%
createBruteForceOnly()
createAlgorithmOnly()
createCombined()