#%%
# Imports
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import numpy as np
import json
import sys

# %%
arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

#%%
# Read XML
durationsSimple = []
states = []
phases = []
durations = []

root = ET.parse(getArg(0, '../596/signals-default.sig.xml')).getroot()
for type_tag in root.findall('tlLogic/phase'):
    durationsSimple.append(int(type_tag.get('duration')))
    states.append(type_tag.get('state'))
  

for state in states:
  phase = []
  for signal in state:
    phase.append(signal)
  phases.append(phase)

# %%
signals = []

with open(getArg(1, '../596/layout.json')) as json_file:
    layout = json.load(json_file)

for edge in layout['edges']:
  for lane in edge['lanes']:
    for signal in lane['signals']:
      signals.append(str(signal['id']) + " " + signal['symbol'].encode('utf-8', 'ignore').decode('utf-8'))

# %%
durations = []
for index, state in enumerate(states):
  phaseDuration = []
  for signal in signals:
    phaseDuration.append(durationsSimple[index])
  durations.append(phaseDuration)

colors = []
widths = []
for phase in phases:
  phaseColors = []
  phaseWidths = []
  for state in phase:
    color = '#eb5160'
    width = .1
    if state == 'u':
      color = '#faa307'
      width = 0.25
    elif state == 'y':
      color = '#f0c808'
      width = 0.25
    elif state == 'g':
      color = '#226f54'
      width = 0.25
    elif state == 'G':
      color = '#06d6a0'
      width = 0.25
    phaseColors.append(color)
    phaseWidths.append(width)
  colors.append(phaseColors)
  widths.append(phaseWidths)

#%%

def addDurations(index):
  return [sum(x) for x in zip(*durations[0:index + 1])]

plt.rc('ytick', labelsize=18)
plt.rc('xtick', labelsize=18)
fig = plt.figure(figsize=(25,10))
ax = fig.add_subplot(111)
for i in range(0, len(durations)):
  if i > 0:
    ax.barh(signals, durations[i], align='center', height=widths[i], color=colors[i], left=addDurations(i-1), label='')
  else:
    ax.barh(signals, durations[i], align='center', height=widths[i], color=colors[i],label='')
ax.set_xticks(np.arange(0, addDurations(len(durations) - 1)[0] + 5, step=5))
ax.set_xlabel('Seconds')
ax.set_ylabel('Signals')
#ax.set_title('Signal Plan')
#ax.grid(True)
plt.gca().invert_yaxis()
plt.savefig('./' + getArg(2, 'signal-plan.eps'))
#plt.show()


# %%
