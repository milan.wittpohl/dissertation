# %%
import pandas as pd
import matplotlib.pyplot as plt
import sys

arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

ident = getArg(0, 'PS-V')

# %%
print('Evaluating Results...')

df = pd.read_table('./' + str(ident) + '/results-ea-batch-' + str(ident) + '-results.csv', sep=";")
df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))

df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]

# avg. chromosomes
print('Avg Chromosomes evaluated: %.2f' % df['Chromosomes'].mean())

# avg. Fitness
print('Avg Fitness: %.2f' % df_validFitness['Fitness'].mean())
print('Max Fitness: %.2f' % df_validFitness['Fitness'].max())
print('Min Fitness: %.2f' % df_validFitness['Fitness'].min())
print('Avg. Generation: %.2f' % df_validFitness['GenerationFound'].mean())
print('Chromosomes with valid fitness: ' + str(df_validFitness.shape[0]))
print('Invalid Fitness: ' + str(df['Fitness'].shape[0] - df_validFitness.shape[0]))


# %%
fig = plt.figure(figsize=(10,1))
ax=fig.add_axes([0,0,1,1])
bp_dict = ax.boxplot(df_validFitness['Fitness'], vert=False, showmeans=True)
ax.set_xlabel('Fitness')
ax.set_ylabel('')
ax.set_yticklabels(['Batch-Run (' + str(int(round(df_validFitness.shape[0]))) + ')'])

for index,line in enumerate(bp_dict['medians']):
    x, y = line.get_xydata()[1]
    ax.annotate('%.1f' % x, xy=(x, y + 0.1))

for index,line in enumerate(bp_dict['means']):
    x, y = line.get_xydata()[0]
    ax.annotate('%.1f' % x, xy=(x, y - .3))

for index,line in enumerate(bp_dict['caps']):
    x, y = line.get_xydata()[0]
    ax.annotate('%.1f' % x, xy=(x, y - .3))

for index,line in enumerate(bp_dict['whiskers']):
    x, y = line.get_xydata()[0]
    ax.annotate('%.1f' % x, xy=(x, y - .3))

plt.savefig('./' + str(ident) + '/' + str(ident) + '-results-box-plot.pdf', bbox_inches='tight')
#plt.show()

# %%
'''
print('Evaluting evolution...')

df = pd.read_table('results-ea-batch-' + str(ident) + '-evolution.csv', sep=";")
fig = plt.figure()
#plt.axis([0, 100, 1, 1])
plt.plot(df['Generation'], df['MeanFitness'], c="darkblue")
plt.savefig(str(ident) + '-evolution.pdf', bbox_inches='tight')

print('Evaluting distances...')

df = pd.read_table('results-ea-batch-' + str(ident) + '-distances.csv', sep=";")
fig = plt.figure()
#plt.axis([0, 100, 1, 1])
plt.plot(df['Generation'], df['MeanDistance'], c="darkblue")
plt.savefig(str(ident) + '-distance.pdf', bbox_inches='tight')
'''
print('Creating combined plot...')
df_e = pd.read_table('./' + str(ident) + '/results-ea-batch-' + str(ident) + '-evolution.csv', sep=";")
df_d = pd.read_table('./' + str(ident) + '/results-ea-batch-' + str(ident) + '-distances.csv', sep=";")

fig, ax1 = plt.subplots()

color = '#0077b6'
ax1.set_xlabel('Generation')
ax1.set_ylabel('Mean Best Fitness', color=color)
ax1.plot(df_e['Generation'], df_e['MeanFitness'], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = '#03045e'
ax2.set_ylabel('Mean Distance', color=color)  # we already handled the x-label with ax1
ax2.plot(df_e['Generation'], df_d['MeanDistance'], color=color)
ax2.tick_params(axis='y', labelcolor=color)

plt.axvline(x=df_validFitness['GenerationFound'].mean(), c="#000000", linestyle="--", linewidth=1)
plt.text(df_validFitness['GenerationFound'].mean() + 2, df_d['MeanDistance'].max() - 2, "Best Chromosome\nFound", dict(size=8, color='#000000'))
#plt.text(-15, df_d['MeanDistance'].min() * 0.7, ident, dict(size=8, color='gray'))

plt.savefig('./' + str(ident) + '/' + str(ident) + '-combined.pdf', bbox_inches='tight')