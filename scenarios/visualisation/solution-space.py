#%%

# imports
import math

# Calculate Solution Space

def calculateSolutionSpace(minPhases, maxPhases, minPhaseDuration, maxPhaseDuration, states, printSolutions, printTimes):

  durationStepSize = 1

  localTimePersimulation = 84
  cloudTimePersimulation = 20

  numberOfPossibleSignalLengths = maxPhases - minPhases + 1
  numberOfPossibleDurations = math.ceil((maxPhaseDuration - minPhaseDuration + 1) / durationStepSize)
  
  # Solution Space
  numberOfAllSolutions = 0
  numberOfValidSolutions = 0
  numberOfInvalidSolutions = 0

  for i in range(minPhases, maxPhases + 1):
    durationFactor = math.pow(numberOfPossibleDurations, i)
    newSolutions = math.pow(states, i) * durationFactor
    validSolutions = states * math.pow((states - 1), (states - 1 - (states - i))) * durationFactor
    invalidSolutions = newSolutions - validSolutions
    numberOfAllSolutions = numberOfAllSolutions + newSolutions
    numberOfValidSolutions = numberOfValidSolutions + validSolutions
    numberOfInvalidSolutions = numberOfInvalidSolutions + invalidSolutions

  if printSolutions:
    print("Number of all solutions: " + f'{numberOfAllSolutions:,}')
    print("Number of valid solutions: " + f'{numberOfValidSolutions:,}')
    print("Number of invalid solutions: " + f'{numberOfInvalidSolutions:,}')

  # Brute Force
  if printTimes:
    print("Time to brute force locally in seconds: " + str(numberOfValidSolutions * localTimePersimulation / 1000))
    print("Time to brute force locally in minutes: " + str(numberOfValidSolutions * localTimePersimulation / 1000 / 60))
    print("Time to brute force locally in hours: " + str(numberOfValidSolutions * localTimePersimulation / 1000 / 60 / 60))
    print("Time to brute force locally in days: " + str(numberOfValidSolutions * localTimePersimulation / 1000 / 60 / 60 / 24))
    print("Time to brute force in the cloud in seconds: " + str(numberOfValidSolutions * cloudTimePersimulation / 1000))
    print("Time to brute force in the cloud in minutes: " + str(numberOfValidSolutions * cloudTimePersimulation / 1000 / 60))
    print("Time to brute force in the cloud in hours: " + str(numberOfValidSolutions * cloudTimePersimulation / 1000 / 60 / 60))
    print("Time to brute force in the cloud in days: " + str(numberOfValidSolutions * cloudTimePersimulation / 1000 / 60 / 60 / 24))

  print(f'{validSolutions/numberOfAllSolutions:,}')

  return [numberOfAllSolutions, numberOfValidSolutions, numberOfInvalidSolutions]


calculateSolutionSpace(4, 8, 5, 60, 6, True, True)
# %%

# Test function

def checkResult(result, values):
  values.append(values[0] - values[1])
  if not result == values:
    print('failed', result, values)

result = calculateSolutionSpace(2, 2, 5, 5, 2, False, False)
checkResult(result, [4, 2])

result = calculateSolutionSpace(3, 3, 5, 5, 2, False, False)
checkResult(result, [8, 2])

result = calculateSolutionSpace(4, 4, 5, 5, 2, False, False)
checkResult(result, [16, 2])

result = calculateSolutionSpace(4, 4, 5, 5, 3, False, False)
checkResult(result, [81, 24])

result = calculateSolutionSpace(2, 2, 5, 5, 3, False, False)
checkResult(result, [9, 6])

result = calculateSolutionSpace(3, 3, 5, 5, 2, False, False)
checkResult(result, [8, 2])

result = calculateSolutionSpace(2, 2, 5, 5, 3, False, False)
checkResult(result, [9, 6])

result = calculateSolutionSpace(3, 3, 5, 5, 3, False, False)
checkResult(result, [27, 12])

result = calculateSolutionSpace(2, 2, 5, 5, 4, False, False)
checkResult(result, [16, 12])

result = calculateSolutionSpace(3, 3, 5, 5, 4, False, False)
checkResult(result, [64, 36])

result = calculateSolutionSpace(4, 4, 5, 5, 4, False, False)
checkResult(result, [256, 108])

result = calculateSolutionSpace(6, 6, 5, 5, 4, False, False)
checkResult(result, [4096, 972])

result = calculateSolutionSpace(2, 4, 5, 5, 4, False, False)
checkResult(result, [336, 156])

result = calculateSolutionSpace(2, 4, 5, 10, 4, False, False)
checkResult(result, [4672, 2064])

result = calculateSolutionSpace(2, 4, 5, 60, 4, False, False)
checkResult(result, [5421312, 2303424])

# Brute Force Test I
result = calculateSolutionSpace(4, 4, 5, 60, 4, False, False)
checkResult(result, [5308416, 2239488])


print('All test were executed.')

# %%
