package DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Wrapper class for a penalty
 */
@Getter
@AllArgsConstructor
public class Penalty {

    public enum Reason{
        JAM, COLLISION, EMERGENCY_BREAK, EMERGENCY_STOP;
    }

    private Reason reason;

}
