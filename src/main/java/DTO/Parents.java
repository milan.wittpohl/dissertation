package DTO;

import common.data.Chromosome;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Wrapper for the parents for a new child
 */
@Getter
public class Parents {

    private static final Logger logger = LogManager.getLogger(Parents.class.getName());

    private Chromosome parentA;
    private Chromosome parentB;

    public Parents(Chromosome parentA, Chromosome parentB) {
        if(parentA.getId().equals(parentB.getId())){
            logger.error("Creating parents with duplicate IDs: " + parentA.getId() + " - " + parentB.getId());
        }
        this.parentA = parentA;
        this.parentB = parentB;
    }
}
