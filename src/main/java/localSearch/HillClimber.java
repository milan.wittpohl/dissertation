package localSearch;

import common.data.Chromosome;
import EA.data.Population;
import common.service.StatisticsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * Local Search by hill climber
 *  -> The five best chromosomes are selected
 *  -> Each chromosomes durations are slightly modified
 *  -> Only if the improved durations perform better the durations are replaced
 */
public class HillClimber {

    private static final Logger logger = LogManager.getLogger(HillClimber.class.getName());

    public static void execute(){
        logger.info("Starting Local Search...");
        Population population = (Population) StatisticsService.result.getFinalPopulation();

        // Loop is aborted by time limit
        while(true){
            List<Chromosome> chromosomeList = new ArrayList<>();;
            Arrays.asList(population.getChromosomes().toArray()).forEach(c -> chromosomeList.add((Chromosome) c));
            List<Chromosome> sorted = chromosomeList.stream().sorted(Comparator.comparingDouble(c -> c.getFitness().getValueRegardlessOfSharedFitness())).collect(Collectors.toList());
            List<Chromosome> fiveBest = sorted.subList(0, 5);
            for(int i = 0; i < 1000; i++){
                for(int n = 0; n < fiveBest.size(); n++){
                    Chromosome chromosome = new Chromosome(fiveBest.get(n));
                    int index = sorted.indexOf(fiveBest.get(n));
                    Double fitnessBefore = chromosome.getFitness().getValueRegardlessOfSharedFitness();
                    chromosome.forceMutationOfDurationByApplyingOffspring();
                    chromosome.evaluate(population);
                    if(chromosome.getFitness().getValueRegardlessOfSharedFitness() < fitnessBefore){
                        sorted.set(index, chromosome);
                        population = new Population(new ConcurrentLinkedQueue<>(sorted));
                        logger.info("Mutation resulted in better fitness.");
                    }
                    logger.info("Mutated " + (i * 5 + n + 1) + " times.");
                    StatisticsService.result.setPopulationValues(population);
                }
            }
        }
    }

}
