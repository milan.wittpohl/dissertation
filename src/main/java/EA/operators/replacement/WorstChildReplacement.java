package EA.operators.replacement;

import common.data.Chromosome;

import java.util.Comparator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WorstChildReplacement implements ReplacementOperator{

    /**
     * Replace worst member of population, if child has better fitness
     * @param chromosomes the chromosomes of the population
     * @param child the child to potentially be placed in the population
     * @return the updated population
     */
    @Override
    public ConcurrentLinkedQueue<Chromosome> replace(ConcurrentLinkedQueue<Chromosome> chromosomes, Chromosome child) {
        Chromosome worst = getWorst(chromosomes);
        if (worst.getFitness().getValue() > child.getFitness().getValue()) {
            chromosomes.remove(worst);
            chromosomes.add(child);
        }
        return chromosomes;
    }

    /**
     *
     * @param chromosomes the chromosomes of the population
     * @return Returns the chromosome of the population with the worst fitness
     */
    public Chromosome getWorst(ConcurrentLinkedQueue<Chromosome> chromosomes){
        return chromosomes.stream().max(Comparator.comparingDouble(c -> c.getFitness().getValue())).get();
    }
}
