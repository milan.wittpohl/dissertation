package EA.operators.replacement;

import common.data.Chromosome;

import java.util.Comparator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class OldestChildReplacement implements ReplacementOperator{

    /**
     * Replace oldest member of population
     * @param chromosomes the chromosomes of the population
     * @param child the child to potentially be placed in the population
     * @return the updated population
     */
    @Override
    public ConcurrentLinkedQueue<Chromosome> replace(ConcurrentLinkedQueue<Chromosome> chromosomes, Chromosome child) {
        Chromosome oldest = getOldest(chromosomes);
        chromosomes.remove(oldest);
        chromosomes.add(child);
        return chromosomes;
    }

    /**
     *
     * @param chromosomes the chromosomes of the population
     * @return Returns the chromosome of the population with the lowest generation born
     */
    public Chromosome getOldest(ConcurrentLinkedQueue<Chromosome> chromosomes){
        return chromosomes.stream().min(Comparator.comparingInt(c -> c.getGenerationBorn())).get();
    }
}
