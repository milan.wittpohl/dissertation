package EA.operators.replacement;

import common.data.Chromosome;

import java.util.concurrent.ConcurrentLinkedQueue;

public class WorstChildWithProbabilityReplacement extends WorstChildReplacement implements ReplacementOperator{

    /**
     * Replace worst member of population, if child has better fitness or in 50% of cases
     * @param chromosomes the chromosomes of the population
     * @param child the child to potentially be placed in the population
     * @return the updated population
     */
    public ConcurrentLinkedQueue<Chromosome> replaceWithProb(ConcurrentLinkedQueue<Chromosome> chromosomes, Chromosome child, Double probabilityReversed) {
        Chromosome worst = getWorst(chromosomes);
        if (worst.getFitness().getValue() > child.getFitness().getValue() || probabilityReversed <= 0.5) {
            chromosomes.remove(worst);
            chromosomes.add(child);
        }
        return chromosomes;
    }

    @Override
    public ConcurrentLinkedQueue<Chromosome> replace(ConcurrentLinkedQueue<Chromosome> chromosomes, Chromosome child) {
        return replaceWithProb(chromosomes, child, Math.random());
    }
}
