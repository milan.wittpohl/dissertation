package EA.operators.replacement;

import common.data.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

public class RandomChildReplacement implements ReplacementOperator{

    private static final Logger logger = LogManager.getLogger(RandomChildReplacement.class.getName());

    /**
     * Replace random member of population
     * @param chromosomes the chromosomes of the population
     * @param child the child to potentially be placed in the population
     * @return the updated population
     */
    @Override
    public ConcurrentLinkedQueue<Chromosome> replace(ConcurrentLinkedQueue<Chromosome> chromosomes, Chromosome child) {
        int randomIndex = ThreadLocalRandom.current().nextInt(0, chromosomes.size());
        List<Chromosome> chromosomeList = new ArrayList<>();;
        Arrays.asList(chromosomes.toArray()).forEach(c -> chromosomeList.add((Chromosome) c));
        Chromosome chromosomeToRemove = chromosomeList.get(randomIndex);
        boolean removed = chromosomes.remove(chromosomeToRemove);
        if(!removed){
            logger.error("Could not remove element.");
        }
        chromosomes.add(child);
        return chromosomes;
    }
}
