package EA.operators.mutation;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ShuffleMutation implements MutationOperator {

    /**
     * Mutates a list by shuffling it
     * @param list the list to mutate
     * @return the mutated signals
     */
    @Override
    public List<Integer> mutate(List<Integer> list) {
        Collections.shuffle(list);
        return list;
    }
}
