package EA.operators.crossover;

import DTO.Parents;
import common.data.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Determine longer and shorter parent
 * Create two random points in longer parent
 * Insert Parent B in parent A
 * Return combination
 */
public class TwoPointCrossover implements CrossoverOperator {

    private static final Logger logger = LogManager.getLogger(TwoPointCrossover.class.getName());

    /**
     * Two Point Crossover of two parents
     * @param parents the parents
     * @return the crossed over durations
     */
    @Override
    public List<Integer> crossoverDurations(Parents parents) {

        Integer lengthA = parents.getParentA().getDurations().size();
        Integer lengthB = parents.getParentB().getDurations().size();
        Chromosome longerParent = lengthB > lengthA ? parents.getParentB() : parents.getParentA();
        Chromosome shorterParent = lengthB > lengthA ? parents.getParentA() : parents.getParentB();

        int randomIndexA = ThreadLocalRandom.current().nextInt(0, longerParent.getDurations().size());

        List<Integer> partAFromLongerParent = longerParent.getDurations().subList(0, randomIndexA);
        List<Integer> partBFromLongerParent = longerParent.getDurations().subList(randomIndexA, longerParent.getDurations().size());
        List<Integer> combination = new ArrayList<>(partAFromLongerParent);
        combination.addAll(shorterParent.getDurations());
        combination.addAll(partBFromLongerParent);

        return combination;
    }

    @Override
    public List<Integer> crossoverSignals(Parents parents) {
        Integer lengthA = parents.getParentA().getSignals().size();
        Integer lengthB = parents.getParentB().getSignals().size();
        Chromosome longerParent = lengthB > lengthA ? parents.getParentB() : parents.getParentA();
        Chromosome shorterParent = lengthB > lengthA ? parents.getParentA() : parents.getParentB();

        int randomIndexA = ThreadLocalRandom.current().nextInt(0, longerParent.getSignals().size());

        List<Integer> partAFromLongerParent = longerParent.getSignals().subList(0, randomIndexA);
        List<Integer> partBFromLongerParent = longerParent.getSignals().subList(randomIndexA, longerParent.getSignals().size());
        List<Integer> combination = new ArrayList<>(partAFromLongerParent);
        combination.addAll(shorterParent.getSignals());
        combination.addAll(partBFromLongerParent);

        return combination;
    }

}
