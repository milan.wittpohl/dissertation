package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;
import configuration.Configuration;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

public class TournamentSelection implements SelectionOperator {

    /**
     * Selects parents by tournament selection
     * @return The parents as list of two chromosomes
     */
    @Override
    public Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes) {
        List<Chromosome> chromosomeList = new ArrayList<>();;
        Arrays.asList(chromosomes.toArray()).forEach(c -> chromosomeList.add((Chromosome) c));
        Collections.shuffle(chromosomeList);
        List<Chromosome> tournament = chromosomeList.subList(0, (Integer) Configuration.ac.selectionOptions().get("tournamentSize"));
        List<Chromosome> tournamentSorted = tournament.stream().sorted(Comparator.comparingDouble(c -> c.getFitness().getValue())).collect(Collectors.toList());
        return new Parents(tournamentSorted.get(0), tournamentSorted.get(1));
    }

}
