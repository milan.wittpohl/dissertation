package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;
import common.service.ChromosomeComparisonService;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Selects parents based on their euclidean distance (has to be within a configured radius)
 */
public class RestrictedByDistanceSelection implements SelectionOperator {

    private static final Logger logger = LogManager.getLogger(RestrictedByDistanceSelection.class.getName());

    @Override
    public Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes) {
        TournamentSelection tournamentSelection = new TournamentSelection();
        Parents parents = tournamentSelection.select(chromosomes);
        Chromosome parentA = parents.getParentA();
        for(Chromosome chromosome : chromosomes){
            double distance = ChromosomeComparisonService.calculateEuclideanDistance(parentA, chromosome);
            if(distance <= (int) Configuration.ac.selectionOptions().get("restrictedMatingMaxDistance") && !chromosome.equals(parentA)){
                parents = new Parents(parentA, chromosome);
                return parents;
            }
        }
        logger.info("Could not find a chromosome that is close enough for restricted mating");
        RandomSelection randomSelection = new RandomSelection();
        Parents randomParents = randomSelection.select(chromosomes);
        Chromosome randomParent = randomParents.getParentA().equals(parentA) ? randomParents.getParentB() : randomParents.getParentA();
        return new Parents(parentA, randomParent);
    }

}
