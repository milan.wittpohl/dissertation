package EA.data;

import DTO.Parents;
import EA.service.CrossoverService;
import common.data.Chromosome;
import common.service.ParallelisationService;
import common.service.StatisticsService;
import common.AbstractPopulation;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * The population -> evolves over time
 */
public class Population extends AbstractPopulation {

    private static final Logger logger = LogManager.getLogger(Population.class.getName());

    /**
     * When creating a new population, all chromosomes are randomly generated
     */
    public Population(){
        for(int i = 0; i < Configuration.ac.populationSize(); i++){
            Chromosome chromosome = new Chromosome(true);
            chromosome.setGenerationBorn(0);
            chromosomes.add(chromosome);
        }
    }

    // The population is a thread safe list of chromosomes
    public Population(ConcurrentLinkedQueue<Chromosome> chromosomes){
        this.chromosomes = chromosomes;
    }

    /**
     * To evaluate the population, each chromosome is evaluated
     * To increase performance this processed is parallelised
     * To avoid concurrency errors the population is not modified during the evaluation
     * Instead the evaluated population replaces the original population in the end
     * Additionally, performance is measured
     */
    public void evaluatePopulation(){
        ConcurrentLinkedQueue<Chromosome> evaluatedChromosomes = new ConcurrentLinkedQueue<>();
        long start = System.nanoTime();
        ParallelisationService.prepareExecution(chromosomes.size());
        chromosomes.forEach(chromosome -> {
            ParallelisationService.execute(() -> {
                chromosome.evaluate(this);
                evaluatedChromosomes.add(chromosome);
            });
        });
        ParallelisationService.finishExecution();
        long end = System.nanoTime();
        long avg = ((end - start) / chromosomes.size()) / 1000000;
        logger.info("Evaluated population with an average of: " + avg);
        chromosomes = evaluatedChromosomes;
        StatisticsService.result.setPopulationValues(this);
    }

    /**
     * Creates a child and evaluates it
     * @param parents the parents
     * @return the new child as chromosome
     */
    public Chromosome makeChild(Parents parents, Integer generation){
        Chromosome child = CrossoverService.crossover(parents);
        child.setGenerationBorn(generation);
        child.mutate();
        child.evaluate(this);
        return child;
    }

}
