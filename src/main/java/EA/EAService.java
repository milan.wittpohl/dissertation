package EA;

import DTO.Result;
import DTO.Fitness;
import EA.data.Population;
import common.service.EvaluationService;
import common.service.StatisticsService;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service for running the EA only containing static methods
 */
public class EAService {

    private static final Logger logger = LogManager.getLogger(EAService.class.getName());
    public static String additionalConfig = null;

    /**
     * Main method to run EA
     * @return the results of the EA
     */
    public static void runEA(){
        StatisticsService.result = new Result();

        // Evaluate scenario with default signal plan
        Fitness fitnessOfDefaultSignals = EvaluationService.evaluateDefault();
        StatisticsService.result.setFitnessOfDefaultSignals(fitnessOfDefaultSignals);

        // Generate Ids (maximum number of chromosomes)
        Configuration.ac.diversityMechanism().generateIds();
        // Creating initial, random population
        logger.info("Creating initial population");
        Population population = new Population();

        logger.info("Evaluating initial population");
        population.evaluatePopulation();

        StatisticsService.addGenerationForAvgFitness(0, population.getBest().getFitness().getValueRegardlessOfSharedFitness());
        StatisticsService.addGenerationForAvgDistance(0, population);

        Population initialPopulation = new Population(population.getChromosomes());
        logger.info("Best: " + population.getBest().getFitness().getValueRegardlessOfSharedFitness());
        StatisticsService.result.setInitialPopulationValues(initialPopulation);

        // Evolve population over i generations and measure performance
        long totalTimeForEvolution = 0;
        boolean addActive = false;
        String initialConfig = Configuration.getIdentifier();
        for(int i = 0; i < Configuration.ac.generations(); i++){
            if((i + 1) % 10 == 0 && i < 89 && additionalConfig != null){
                Configuration.setConfig(addActive ? initialConfig : additionalConfig);
                addActive = !addActive;
                logger.info("Switched Configuration to: " + Configuration.getIdentifier());
            }
            long startTimeOfCurrentEvolution = System.nanoTime();
            logger.info("Evolving generation: " + (i + 1));
            Configuration.ac.diversityMechanism().evolvePopulation(i, population);
            StatisticsService.addGenerationForAvgFitness(i + 1, population.getBest().getFitness().getValueRegardlessOfSharedFitness());
            StatisticsService.addGenerationForAvgDistance(i + 1, population);
            StatisticsService.currentGeneration = i;
            totalTimeForEvolution = totalTimeForEvolution + (System.nanoTime() - startTimeOfCurrentEvolution);
            logger.info("Best: " + population.getBest().getFitness().getValueRegardlessOfSharedFitness());
            StatisticsService.result.setPopulationValues(population);
        }
        logger.info("Finished evolution with an avg. time of: " + totalTimeForEvolution/Configuration.ac.generations()/1000000);

        StatisticsService.result.setPopulationValues(population);
    }

}
