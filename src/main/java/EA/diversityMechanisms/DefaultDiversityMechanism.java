package EA.diversityMechanisms;

import common.data.Chromosome;
import EA.data.Population;
import common.service.StatisticsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentLinkedQueue;

public class DefaultDiversityMechanism extends AbstractDiversityMechanism implements DiversityMechanism {

    private static final Logger logger = LogManager.getLogger(DefaultDiversityMechanism.class.getName());

    /**
     * The evolution over one generation is split into
     *  -> The creation of children (parallelised)
     *  -> The potential replacement of chromosomes in the population by new children
     */
    public void evolvePopulation(int generation, Population population){
        ConcurrentLinkedQueue<Chromosome> children = new ConcurrentLinkedQueue<>();
        defaultMating(children, population, generation);
        defaultReplacement(children, population);
        StatisticsService.result.setPopulationValues(population);
    }
}
