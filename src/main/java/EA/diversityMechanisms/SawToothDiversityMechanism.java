package EA.diversityMechanisms;

import common.data.Chromosome;
import EA.data.Population;
import common.service.ParallelisationService;
import common.service.StatisticsService;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

public class SawToothDiversityMechanism extends AbstractDiversityMechanism implements DiversityMechanism {

    private static final Logger logger = LogManager.getLogger(SawToothDiversityMechanism.class.getName());

    @Override
    public void generateIds() {
        int numberOfInfusions = Configuration.ac.generations() / (int) Configuration.ac.diversityMechanismOptions().get("sawToothGenerations");
        ParallelisationService.generateIds(Configuration.ac.populationSize() + (Configuration.ac.numberOfChildren() * Configuration.ac.generations()) + (numberOfInfusions * (int) Configuration.ac.diversityMechanismOptions().get("sawToothNumberOfChildren")));
    }

    /**
     * The evolution over one generation is split into
     *  -> The creation of children (parallelised)
     *  -> The potential replacement of chromosomes in the population by new children
     */
    public void evolvePopulation(int generation, Population population){
        ConcurrentLinkedQueue<Chromosome> children = new ConcurrentLinkedQueue<>();
        defaultMating(children, population, generation);
        defaultReplacement(children, population);

        if(generation != 0 && (generation + 1) % (int) Configuration.ac.diversityMechanismOptions().get("sawToothGenerations") == 0){
            int infusedChildren = InfusionDiversityMechanism.infusePopulation(population, generation, (int) Configuration.ac.diversityMechanismOptions().get("sawToothNumberOfChildren"));
            logger.info("Saw-Tooth infused Population with " + infusedChildren + " children.");
        }else{
            int removedChildren;
            List<Chromosome> chromosomeList = new ArrayList<>();;
            Arrays.asList(population.getChromosomes().toArray()).forEach(c -> chromosomeList.add((Chromosome) c));
            for(removedChildren = 0; removedChildren < (int) Configuration.ac.diversityMechanismOptions().get("sawToothNumberOfChromosomesToRemove"); removedChildren++){
                int randomIndex = ThreadLocalRandom.current().nextInt(0, population.getChromosomes().size() - removedChildren);
                Chromosome chromosomeToRemove = chromosomeList.get(randomIndex);
                boolean removed = chromosomeList.remove(chromosomeToRemove);
                if(!removed){
                    logger.error("Saw-Tooth could not remove chromosome.");
                }
            }
            logger.info("Removed " + removedChildren + " children.");
        }
        StatisticsService.result.setPopulationValues(population);
    }
}
