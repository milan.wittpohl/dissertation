package EA.diversityMechanisms;

import EA.data.Population;

/**
 * A diversity mechanism determines how a population is evolved
 */
public interface DiversityMechanism {

    void generateIds();

    void evolvePopulation(int generation, Population population);

}
