package EA.diversityMechanisms;

import DTO.Parents;
import common.data.Chromosome;
import EA.data.Population;
import common.service.ParallelisationService;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Most diversity mechanisms do not differ in the default behaviour for generating ids, mating and replacement
 * The diversity mechanism uses the defined operators of the corresponding configuration
 */
abstract class AbstractDiversityMechanism {

    private static final Logger logger = LogManager.getLogger(AbstractDiversityMechanism.class.getName());

    public void generateIds() {
        ParallelisationService.generateIds(Configuration.ac.populationSize() + (Configuration.ac.numberOfChildren() * Configuration.ac.generations()));
    }

    // The selection and crossover of chromosomes is paralised and uses the defined operators
    void defaultMating(ConcurrentLinkedQueue<Chromosome> children, Population population, int generation){
        ParallelisationService.prepareExecution(Configuration.ac.numberOfChildren());
        for(int n = 0; n < Configuration.ac.numberOfChildren(); n++){
            ParallelisationService.execute(() -> {
                Parents parents = Configuration.ac.selectionOperator().select(population.getChromosomes());
                Chromosome child = population.makeChild(parents, generation);
                children.add(child);
            });
        }
        ParallelisationService.finishExecution();

        consistencyCheck(population);
    }

    // The selection and crossover of chromosomes is NOT paralised and uses the defined operators
    void defaultReplacement(ConcurrentLinkedQueue<Chromosome> children, Population population){
        while(true) {
            final Chromosome child = children.poll();
            if (child == null) {
                break;
            }
            Configuration.ac.replacementOperator().replace(population.getChromosomes(), child);
        }

        consistencyCheck(population);

    }

    // Consistency checks are performed to reduce the chance of unnoticed errors
    private void consistencyCheck(Population population){
        for(Chromosome chromosome : population.getChromosomes()){
            for(Chromosome c : population.getChromosomes()){
                if(chromosome.getId().equals(c.getId()) && !chromosome.equals(c)){
                    logger.error("DOUBLE ID - " + chromosome.getId() + " - " + c.getId());
                }
            }
        }
    }

}
