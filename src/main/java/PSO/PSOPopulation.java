package PSO;

import common.AbstractPopulation;
import common.data.Chromosome;
import common.service.ParallelisationService;
import common.service.StatisticsService;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Population of particles for the PSO
 */
public class PSOPopulation extends AbstractPopulation {

    private static final Logger logger = LogManager.getLogger(PSOPopulation.class.getName());

    @Getter
    ConcurrentLinkedQueue<Particle> particles = new ConcurrentLinkedQueue<>();

    // Main constructor, random initialisation
    PSOPopulation(int n){
        for(int i = 0; i < n; i++){
            particles.add(new Particle());
        }
    }

    // Evaluates all particles of the population
    void evaluate(){
        ParallelisationService.prepareExecution(particles.size());
        for(Particle particle : particles){
            ParallelisationService.execute(particle::evaluate);
        }
        ParallelisationService.finishExecution();
    }

    // Get all chromosomes (thread-safe)
    @Override
    public ConcurrentLinkedQueue<Chromosome> getChromosomes() {
        ConcurrentLinkedQueue<Chromosome> chromosomes = new ConcurrentLinkedQueue<>();
        particles.forEach(p -> chromosomes.add(p.getChromosome()));
        return chromosomes;
    }

    /**
     * Get currently best particle
     * @return the chromosome of the best particle
     */
    Chromosome getBestLocal(){
        Optional<Particle> best = particles.stream().min(Comparator.comparingDouble(p -> {
            if(p.getPersonalBest() == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return p.getChromosome().getFitness().getValue();
        }));
        if(best.isPresent()){
            return best.get().getChromosome();
        }
        logger.error("Could not get best chromosome.");
        return chromosomes.peek();
    }

    /**
     * Get overall best that was ever visited
     * @return the chromosome of the best particle
     */
    @Override
    public Chromosome getBest(){
        Optional<Particle> best = particles.stream().min(Comparator.comparingDouble(p -> {
            if(p.getPersonalBest() == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return p.getPersonalBest().getFitness().getValue();
        }));
        if(best.isPresent()){
            return best.get().getPersonalBest();
        }
        logger.error("Could not get best chromosome.");
        return chromosomes.peek();
    }

    /**
     * Method to get best performing chromosome for the target vehicle of population
     * Safety measures are in place, in case null pointed exceptions happen
     * @return Returns the chromosome of the population with the best time for the target vehicle
     */
    @Override
    public Chromosome getBestForTargetVehicle(){
        Optional<Particle> best = particles.stream().min(Comparator.comparingDouble(p -> {
            if(p.getPersonalBest() == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return p.getPersonalBest().getFitness().getTimeForTargetVehicle();
        }));
        if(best.isPresent()){
            return best.get().getPersonalBest();
        }
        logger.error("Could not get best for target vehicle chromosome.");
        return chromosomes.peek();
    }

    /**
     * Method to get best performing chromosome for the all vehicles of population
     * Safety measures are in place, in case null pointed exceptions happen
     * @return Returns the chromosome of the population with the best time for all vehicles
     */
    @Override
    public Chromosome getBestForAllVehicles(){
        Optional<Particle> best = particles.stream().min(Comparator.comparingDouble(p -> {
            if(p.getPersonalBest() == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return p.getPersonalBest().getFitness().getTimeForAllVehicles();
        }));
        if(best.isPresent()){
            return best.get().getPersonalBest();
        }
        logger.error("Could not get best for all vehicles chromosome.");
        return chromosomes.peek();
    }

}
