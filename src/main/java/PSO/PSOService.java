package PSO;

import DTO.Fitness;
import DTO.Result;
import common.service.EvaluationService;
import common.service.ParallelisationService;
import common.service.StatisticsService;
import configuration.Configuration;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service to run a PSO
 *  -> A population is randomly generated
 *  -> Each particle is evaluated
 *  -> The velocities are calculated
 *  -> Particles are moved and the process repeats (from step two)
 */
public class PSOService {

    private static final Logger logger = LogManager.getLogger(PSOService.class.getName());

    private static PSOPopulation population;

    public static void runPSO(){
        StatisticsService.result = new Result();

        // Evaluate scenario with default signal plan
        Fitness fitnessOfDefaultSignals = EvaluationService.evaluateDefault();
        StatisticsService.result.setFitnessOfDefaultSignals(fitnessOfDefaultSignals);

        ParallelisationService.generateIds(Configuration.ac.populationSize());

        logger.info("Creating initial population");
        population = new PSOPopulation(Configuration.ac.populationSize());
        population.evaluate();
        StatisticsService.result.setInitialPopulationValues(population);
        StatisticsService.addGenerationForAvgFitness(0, population.getBest().getFitness().getValueRegardlessOfSharedFitness());
        StatisticsService.addGenerationForAvgDistance(0, population);

        // Evolve population over i generations and measure performance
        long totalTimeForEvolution = 0;
        for(int i = 0; i < Configuration.ac.generations(); i++){
            long startTimeOfCurrentEvolution = System.nanoTime();
            logger.info("Evolving generation: " + (i + 1));

            updateVelocitiesOfParticles();
            moveParticles();
            population.evaluate();
            StatisticsService.result.setPopulationValues(population);
            StatisticsService.addGenerationForAvgFitness(i + 1, population.getBest().getFitness().getValueRegardlessOfSharedFitness());
            StatisticsService.addGenerationForAvgDistance(i + 1, population);
            StatisticsService.currentGeneration = i;
            totalTimeForEvolution = totalTimeForEvolution + (System.nanoTime() - startTimeOfCurrentEvolution);
            logger.info("Best: " + population.getBest().getFitness().getValueRegardlessOfSharedFitness() + " Local Best: " + population.getBestLocal().getFitness().getValueRegardlessOfSharedFitness());
            StatisticsService.result.setPopulationValues(population);

        }
    }

    private static void updateVelocitiesOfParticles(){
        ParallelisationService.prepareExecution(Configuration.ac.populationSize());
        for(Particle particle : population.particles){
            ParallelisationService.execute(() -> particle.updateVelocity(population));
        }
        ParallelisationService.finishExecution();
    }

    private static void moveParticles(){
        ParallelisationService.prepareExecution(Configuration.ac.populationSize());
        for(Particle particle : population.particles){
            ParallelisationService.execute(particle::move);
        }
        ParallelisationService.finishExecution();
    }

}
