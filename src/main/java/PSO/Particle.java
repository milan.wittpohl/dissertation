package PSO;

import common.data.Chromosome;
import common.service.ChromosomeComparisonService;
import common.service.RepairService;
import configuration.Configuration;
import configuration.algorithm.PSOAlgorithmConfiguration;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A particle for PSO
 * Each particle can either be calm or active, setting the importance of the neighborhoods best particle
 */
public class Particle {

    public enum PsoConfig{
        CALM, ACTIVE
    }

    // Max velocity for the signal
    private Double vMaxSignal;
    // Max velocity for the duration
    private Double vMaxDuration;
    // Weight of personal/neighborhood best
    private Double weightPersonalBest;
    private Double weightLocalBest;

    // A particle consists of a chromosome to manage, fitness, evaluation, comparision etc.
    @Getter
    private Chromosome chromosome;

    // The best position (chromosome) visited by this particle
    @Getter
    private Chromosome personalBest;

    private List<Double> velocitySignals = new ArrayList<>();
    private List<Double> velocityDurations = new ArrayList<>();

    /**
     * Main Constructor
     * Sets max velocity and weights based on configuration
     */
    Particle() {

        PsoConfig psoConfig = ((PSOAlgorithmConfiguration) Configuration.ac).psoConfig();
        if(psoConfig.equals(PsoConfig.CALM)){
            vMaxSignal = 1.0;
            vMaxDuration = 5.0;
            weightPersonalBest = 1.0;
            weightLocalBest = 3.0;
        }else if (psoConfig.equals(PsoConfig.ACTIVE)){
            vMaxSignal = 2.0;
            vMaxDuration = 20.0;
            weightPersonalBest = 1.0;
            weightLocalBest = 3.0;
        }

        chromosome = new Chromosome(true);
        chromosome.setGenerationBorn(0);

        for(int i = 0; i < chromosome.getDurations().size(); i++){
            velocitySignals.add((double) ThreadLocalRandom.current().nextInt(0, Configuration.sc.validSequences().size()));
            velocityDurations.add((double) ThreadLocalRandom.current().nextInt(Configuration.rc.minPhaseDuration(), Configuration.rc.maxPhaseDuration() + 1));
        }
    }

    // Evaluates the particle and updates personal best
    void evaluate(){
        chromosome.evaluate(null);
        if(personalBest == null){
            personalBest = new Chromosome(chromosome);
        }else if(chromosome.getFitness().getValueRegardlessOfSharedFitness() < personalBest.getFitness().getValueRegardlessOfSharedFitness()){
            personalBest = new Chromosome(chromosome);
        }
    }

    /**
     * Updates the velocity of the signals and durations
     *  -> first the local/neighborhoods best particle is determined
     * @param population the population of particles
     */
    void updateVelocity(PSOPopulation population){
        Chromosome localBest = chromosome;
        for(Particle p : population.getParticles()){
            Double distance = ChromosomeComparisonService.calculateEuclideanDistance(chromosome, p.getChromosome());
            if(distance < Configuration.ac.sharedFitnessRadius() &&
               p.getChromosome().getFitness().getValueRegardlessOfSharedFitness() <= localBest.getFitness().getValueRegardlessOfSharedFitness() &&
               !p.getChromosome().equals(chromosome)){
                localBest = p.getChromosome();
            }
        }

        // Sets velocity for durations
        List<Integer> personalDurationDiff = calcDiff(chromosome.getDurations(), personalBest.getDurations());
        List<Integer> localDurationDiff = calcDiff(chromosome.getDurations(), localBest.getDurations());
        calcAddVelocityDurations(personalDurationDiff, weightPersonalBest);
        calcAddVelocityDurations(localDurationDiff, weightLocalBest);

        // Sets velocity for signals
        List<Integer> personalSignalDiff = calcDiff(chromosome.getSignals(), personalBest.getSignals());
        List<Integer> localSignalDiff = calcDiff(chromosome.getSignals(), localBest.getSignals());
        calcAddVelocitySignals(personalSignalDiff, weightPersonalBest);
        calcAddVelocitySignals(localSignalDiff, weightLocalBest);
    }

    // Moves the particle in the search space
    void move(){
        for(int i = 0; i < velocitySignals.size(); i++){
            if(i < chromosome.getSignals().size()){
                Integer currentSignal = chromosome.getSignals().get(i);
                chromosome.getSignals().set(i, (int) Math.round(currentSignal + velocitySignals.get(i)));
            }else{
                chromosome.getSignals().add((int) Math.round(velocitySignals.get(i)));
            }
        }

        for(int i = 0; i < velocityDurations.size(); i++){
            if(i < chromosome.getDurations().size()){
                Integer currentSignal = chromosome.getDurations().get(i);
                chromosome.getDurations().set(i, (int) Math.round(currentSignal + velocityDurations.get(i)));
            }else{
                chromosome.getDurations().add((int) Math.round(velocityDurations.get(i)));
            }
        }
        chromosome.setDurations(RepairService.repairDurations(chromosome.getDurations()));
        chromosome.setSignals(RepairService.repairSignals(chromosome.getSignals()));
        chromosome.setGenerationBorn(chromosome.getGenerationBorn() + 1);
    }

    private void calcAddVelocitySignals(List<Integer> diffs, Double weight){
        for(int i = 0; i < diffs.size(); i++){
            Double velocityToAdd = diffs.get(i) * Math.random() * weight;
            if(i < velocitySignals.size()){
                Double currentVelocity = velocitySignals.get(i);
                double newVelocity = currentVelocity + velocityToAdd;
                velocitySignals.set(i, Math.min(newVelocity, vMaxSignal));
            }else{
                velocitySignals.add(Math.min(velocityToAdd, vMaxSignal));
            }
        }
    }

    private void calcAddVelocityDurations(List<Integer> diffs, Double weight){
        for(int i = 0; i < diffs.size(); i++){
            Double velocityToAdd = diffs.get(i) * Math.random() * weight;
            if(i < velocityDurations.size()){
                Double currentVelocity = velocityDurations.get(i);
                double newVelocity = currentVelocity + velocityToAdd;
                velocityDurations.set(i, Math.min(newVelocity, vMaxDuration));
            }else{
                velocityDurations.add(Math.min(velocityToAdd, vMaxDuration));
            }
        }
    }

    private List<Integer> calcDiff(List<Integer> listA, List<Integer> listB){
        List<Integer> diff = new ArrayList<>();
        for(int i = 0; i < listB.size(); i++){
            int d;
            if(i < listA.size()){
                d = listB.get(i) - listA.get(i);
            }else{
                d = listB.get(i);
            }
            diff.add(d);
        }
        return diff;
    }

}
