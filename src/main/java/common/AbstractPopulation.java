package common;

import common.data.Chromosome;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

// Abstract class for a population of chromosomes (EA) or particles (PSO)
public abstract class AbstractPopulation {

    private static final Logger logger = LogManager.getLogger(AbstractPopulation.class.getName());

    // The members, chromosomes of the population
    @Getter
    @Setter
    protected ConcurrentLinkedQueue<Chromosome> chromosomes = new ConcurrentLinkedQueue<>();

    /**
     * Method to get best performing chromosome of population
     * Safety measures are in place, in case null pointed exceptions happen
     * @return Returns the chromosome of the population with the best fitness
     */
    public Chromosome getBest(){
        Optional<Chromosome> best = chromosomes.stream().min(Comparator.comparingDouble(c -> {
            if(c == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return c.getFitness().getValue();
        }));
        if(best.isPresent()){
            return best.get();
        }
        logger.error("Could not get best chromosome.");
        return chromosomes.peek();
    }

    /**
     * Method to get best performing chromosome for the target vehicle of population
     * Safety measures are in place, in case null pointed exceptions happen
     * @return Returns the chromosome of the population with the best time for the target vehicle
     */
    public Chromosome getBestForTargetVehicle(){
        Optional<Chromosome> best = chromosomes.stream().min(Comparator.comparingDouble(c -> {
            if(c == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return c.getFitness().getTimeForTargetVehicle();
        }));
        if(best.isPresent()){
            return best.get();
        }
        logger.error("Could not get best for target vehicle chromosome.");
        return chromosomes.peek();
    }

    /**
     * Method to get best performing chromosome for the all vehicles of population
     * Safety measures are in place, in case null pointed exceptions happen
     * @return Returns the chromosome of the population with the best time for all vehicles
     */
    public Chromosome getBestForAllVehicles(){
        Optional<Chromosome> best = chromosomes.stream().min(Comparator.comparingDouble(c -> {
            if(c == null){
                logger.error("chromosomes is null");
                return Double.MAX_VALUE;
            }
            return c.getFitness().getTimeForAllVehicles();
        }));
        if(best.isPresent()){
            return best.get();
        }
        logger.error("Could not get best for all vehicles chromosome.");
        return chromosomes.peek();
    }

}
