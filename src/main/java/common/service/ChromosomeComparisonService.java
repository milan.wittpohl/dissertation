package common.service;

import common.data.Chromosome;

import java.util.List;

/**
 * This service is used to compare Chromosomes
 *  -> Currently, this is achieved by calculating the euclidean distance for both durations and signals
 *  -> The distances are calculated separately and then added
 */
public class ChromosomeComparisonService {

    /**
     * calculates combined distance
     * @param chromosomeA first chromosome
     * @param chromosomeB second chromosome
     * @return the euclidean distance
     */
    public static Double calculateEuclideanDistance(Chromosome chromosomeA, Chromosome chromosomeB){
        Double differenceInDuration = calculateEuclideanDistanceForList(chromosomeA.getDurations(), chromosomeB.getDurations());
        Double differenceInSignals = calculateEuclideanDistanceForList(chromosomeA.getSignals(), chromosomeB.getSignals());
        return differenceInDuration + differenceInSignals;
    }

    /**
     * Calculates the distance for a list of integers (signals or durations)
     * Source: https://link-springer-com.ezproxy.napier.ac.uk/referenceworkentry/10.1007/978-1-4020-6754-9_5603
     * Only public for tests
     * @param listA first list
     * @param listB second list
     * @return the euclidean distance
     */
    public static double calculateEuclideanDistanceForList(List<Integer> listA, List<Integer> listB){
        List<Integer> longerList = listA.size() > listB.size() ? listA : listB;
        List<Integer> shorterList = listA.size() > listB.size() ? listB : listA;

        double sum = 0.0;

        for(int i = 0; i < longerList.size(); i++){
            Integer x = longerList.get(i);
            Integer y = -1;
            if(i < shorterList.size()){
                y = shorterList.get(i);
            }
            int difference = x - y;
            sum = sum + Math.pow(difference, 2);
        }

        return Math.sqrt(sum);
    }

}
