package common.service;

import java.io.File;
import java.nio.file.Paths;

// Clean up after application finished running
public class CleanUpService {

    // Delete all XMLs in /scenarios/tmp/signals, /scenarios/tmp/trips and /scenarios/tmp/errors
    public static void cleanUp(){
        File signalsDirectory = new File(Paths.get(".").toAbsolutePath().normalize().toString()  + "/scenarios/tmp/signals/");
        File tripsDirectory = new File(Paths.get(".").toAbsolutePath().normalize().toString()  + "/scenarios/tmp/trips/");
        File errorsDirectory = new File(Paths.get(".").toAbsolutePath().normalize().toString()  + "/scenarios/tmp/errors/");
        deleteAllFilesInDirectory(signalsDirectory);
        deleteAllFilesInDirectory(tripsDirectory);
        deleteAllFilesInDirectory(errorsDirectory);
    }

    private static void deleteAllFilesInDirectory(File directory){
        for(File file: directory.listFiles())
            if (!file.isDirectory())
                file.delete();
    }

}
