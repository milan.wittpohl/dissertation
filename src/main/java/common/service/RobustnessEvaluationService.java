package common.service;

import DTO.Fitness;
import DTO.SimulationResults;
import common.data.Chromosome;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This service runs a given EA configuration n times
 * This is used to evaluate the robustness over increasing traffic saturation
 */
public class RobustnessEvaluationService {


    private static final Logger logger = LogManager.getLogger(RobustnessEvaluationService.class.getName());
    private static String resultsFileName = "results-ea-robustness-" + Configuration.getIdentifier() + "-results.csv";
    public static int currentEvaluation = 0;

    /**
     * Evaluates the default plan n times
     * @param numberOfEvaluations number of evaluations
     * @param defaultConfig the configuration to use
     * @param switchConfig the secondary config to switch to (optional)
     */
    public static void prepareAndExecuteEA(int numberOfEvaluations, String defaultConfig, String switchConfig){
        StatisticsService.deleteFile(resultsFileName);
        StatisticsService.writeTxt(resultsFileName, "Run;Chromosomes;GenerationFound;Fitness;Time-VIP;Time-All;Duration-Sum;Signal-Index;Durations;Signals\n");
        for(int i = 1; i <= numberOfEvaluations; i++){
            logger.info("Running Evaluation: " + i + "/" + numberOfEvaluations);
            long start = System.nanoTime();
            ExecutionService.runEA(start, switchConfig);
            writeBest(i, StatisticsService.result.getBestOverall());
            Configuration.setConfig(defaultConfig);
            try {
                // Used to eliminate occasional exceptions
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentEvaluation++;
        }
    }

    /**
     * Evaluates the default plan n times
     * @param numberOfEvaluations number of evaluations
     * @param defaultConfig the configuration to use
     */
    public static void prepareAndExecuteDefault(int numberOfEvaluations, String defaultConfig){
        StatisticsService.deleteFile(resultsFileName);
        StatisticsService.writeTxt(resultsFileName, "Run;Fitness;Time-VIP;Time-All\n");
        for(int i = 1; i <= numberOfEvaluations; i++){
            logger.info("Running Default Evaluation: " + i + "/" + numberOfEvaluations);
            Fitness fitness = EvaluationService.evaluateDefault();
            writeBest(i, fitness);
            Configuration.setConfig(defaultConfig);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentEvaluation++;
        }
    }

    // Export best signal plan of EA/PSO result
    private static void writeBest(int run, Chromosome best){
        String str = run + ";-1;-1;-1;-1;-1;-1;-1;-1;-1\n";
        try{
            str = run + ";" + StatisticsService.result.getEvaluatedChromosomes() + ";" + best.getGenerationBorn() + ";" + best.getFitness().getValue() + ";" + best.getFitness().getTimeForTargetVehicle() + ";" + best.getFitness().getTimeForAllVehicles() + ";" + best.getDurations().stream().mapToInt(Integer::intValue).sum() + ";" + BruteForceService.getSignalIndex(best) + ";" + best.getDurations().toString() + ";" + best.getSignals().toString() + "\n";
        }catch (Exception e){
            e.printStackTrace();
        }
        StatisticsService.writeTxt(resultsFileName, str);
    }

    // Export results of default signal plan
    private static void writeBest(int run, Fitness fitness){
        String str = run + ";-1;-1;-1\n";
        try{
            str = run + ";" + fitness.getValueRegardlessOfSharedFitness() + ";" + fitness.getTimeForTargetVehicle() + ";" + fitness.getTimeForAllVehicles() + "\n";
        }catch (Exception e){
            e.printStackTrace();
        }
        StatisticsService.writeTxt(resultsFileName, str);
    }



}
