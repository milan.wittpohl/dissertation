package common.service;

import DTO.Result;
import DTO.Fitness;
import common.data.Chromosome;
import EA.data.Population;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import transformers.ChromosomeTransformer;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * The brute run service can be used to evaluate all or some possible solutions for a given configuration
 * The results are written to a file
 * The execution can either be performed by
 *  -> randomly generating chromosomes or
 *  -> by systematically evaluating all possible solutions
 * See readme in root folder for more details
 */
public class BruteForceService {

    private static final Logger logger = LogManager.getLogger(BruteForceService.class.getName());

    // The filename for the results, dependent on the given configuration
    private static final String resultsFile = "results-" + Configuration.getIdentifier() + ".csv";

    // When not executing randomly, the duration step size defines if every (1), every fifth (5), every n-th (n) solution should be evaluated
    private static final Integer durationStepSize = 5;


    public static void prepareAndExecute(int batchSize, int numberOfRandoms){
        logger.info("Starting Brute Force Execution...");

        StatisticsService.result = new Result();

        // Evaluate scenario with default signal plan
        Fitness fitnessOfDefaultSignals = EvaluationService.evaluateDefault();
        StatisticsService.result.setFitnessOfDefaultSignals(fitnessOfDefaultSignals);

        if(numberOfRandoms > 0){
            executeRandomly(batchSize, numberOfRandoms);
        }else{
            execute(batchSize);
        }

        // Print various statistics
        logger.info("Statistics:");
        logger.info("\t\tDefault Signal Plan with fitness: " + StatisticsService.result.getFitnessOfDefaultSignals().getValue() + " - time for target vehicle: " + StatisticsService.result.getFitnessOfDefaultSignals().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getFitnessOfDefaultSignals().getTimeForAllVehicles());
        logger.info("\t\tBest Overall result with fitness: " + StatisticsService.result.getBestOverall().getFitness().getValue() + " - time for target vehicle: " + StatisticsService.result.getBestOverall().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestOverall().getFitness().getTimeForAllVehicles());
        logger.info("\t\tBest Target Vehicle result with fitness: " + StatisticsService.result.getBestForTargetVehicle().getFitness().getValue() + " - time for target vehicle: " + StatisticsService.result.getBestForTargetVehicle().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestForTargetVehicle().getFitness().getTimeForAllVehicles());
        logger.info("\t\tBest All Vehicles result with fitness: " + StatisticsService.result.getBestForAllVehicles().getFitness().getValue() + " - time for target vehicle: " + StatisticsService.result.getBestForAllVehicles().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestForAllVehicles().getFitness().getTimeForAllVehicles() + "\n");
    }

    /**
     * Random execution
     *  -> N random chromosomes are generated and evaluated in batch sizes
     *  -> Evaluated chromosomes are written to a predefined result file
     * @param batchSize the batch size
     * @param numberOfRandoms the total number of randoms to generate and evaluate
     */
    private static void executeRandomly(int batchSize, int numberOfRandoms){
        StatisticsService.deleteFile(resultsFile);
        StatisticsService.writeTxt(resultsFile, "Fitness;Time-VIP;Time-All;Duration-Sum;Signal-Index;Durations;Signals\n");
        ParallelisationService.generateIds(numberOfRandoms);
        for(int i = 0; i < numberOfRandoms; i = i + batchSize){
            ConcurrentLinkedQueue<Chromosome> toEvaluate = new ConcurrentLinkedQueue<>();
            for(int n = 0; n < batchSize; n++){
                Chromosome chromosome = new Chromosome(true);
                toEvaluate.add(chromosome);
            }
            Population population = new Population(toEvaluate);
            population.evaluatePopulation();
            for(Chromosome chromosome : population.getChromosomes()){
                Double f = chromosome.getFitness().getValue();
                if(f >= 1000000.0){
                    f = 0.0;
                    System.out.println("Double was max value");
                }
                String str = f + ";" + chromosome.getFitness().getTimeForTargetVehicle() + ";" + chromosome.getFitness().getTimeForAllVehicles() + ";" + chromosome.getDurations().stream().mapToInt(Integer::intValue).sum() + ";-1;" + chromosome.getDurations().toString() + ";" + chromosome.getSignals().toString() + "\n";
                StatisticsService.writeTxt(resultsFile, str);
            }
            CleanUpService.cleanUp();
        }
    }

    /**
     * Non-random execution which systematically generates all possible solutions
     * Depending on the step size every n-th solution is evaluated
     * @param batchSize the batch size
     */
    private static void execute(int batchSize) {
        logger.info("Creating all possible solutions...");
        List<List<Integer>> allSolutions = buildSolutions();
        logger.info("Number of all possible solutions: " + allSolutions.size());
        logger.info("Removing unwanted solutions...");
        List<List<Integer>> wantedSolutions = removeUnwantedSolutions(allSolutions);
        logger.info("Number of wanted solutions: " + wantedSolutions.size());
        logger.info("Removing invalid solutions...");
        List<List<Integer>> solutions = removeInvalidSolutions(wantedSolutions);
        logger.info("Number of valid solutions: " + solutions.size());
        logger.info("Building wanted / valid chromosomes...");
        List<Chromosome> chromosomes = buildChromosomes(solutions);
        logger.info("Building all chromosomes...");
        List<Chromosome> allChromosomes = buildChromosomesAndMatch(allSolutions, chromosomes);
        List<Chromosome> wantedChromosomes = buildChromosomesAndMatch(wantedSolutions, chromosomes);
        logger.info("Created " + chromosomes.size() + " chromosomes.");

        logger.info("Evaluating population...\n");
        ConcurrentLinkedQueue<Chromosome> evaluatedChromosomes = new ConcurrentLinkedQueue<>();
        int batch = 1;
        for(int i = 0; i < chromosomes.size(); i = i + batchSize){
            logger.info("Running batch: " + batch + "/" + Math.ceil((double) chromosomes.size() / (double) batchSize));
            ConcurrentLinkedQueue<Chromosome> supPopulationChromosomes = new ConcurrentLinkedQueue<>();
            for(int n = i; n < i + batchSize; n++){
                if(n < chromosomes.size())
                    supPopulationChromosomes.add(chromosomes.get(n));
            }
            Population subPopulation = new Population(supPopulationChromosomes);
            subPopulation.evaluatePopulation();
            evaluatedChromosomes.addAll(supPopulationChromosomes);
            Population evaluatedChromosomePopulation = new Population(evaluatedChromosomes);
            StatisticsService.result.setPopulationValues(evaluatedChromosomePopulation);

            ChromosomeTransformer.transformChromosome(StatisticsService.result.getBestOverall(), "best-overall");
            ChromosomeTransformer.transformChromosome(StatisticsService.result.getBestForTargetVehicle(), "best-target-vehicle");
            ChromosomeTransformer.transformChromosome(StatisticsService.result.getBestForAllVehicles(), "best-all-vehicles");

            CleanUpService.cleanUp();
            batch++;
        }

        printSolutions(allChromosomes, wantedChromosomes, evaluatedChromosomes);
    }

    // Systematically builds all possible solutions
    public static List<List<Integer>> buildSolutions(){
        List<Integer> durations = new ArrayList<>();
        List<Integer> states = new ArrayList<>();

        for(int d = Configuration.rc.minPhaseDuration(); d <= Configuration.rc.maxPhaseDuration(); d++){
            durations.add(d);
        }

        for(int s = 0; s < Configuration.sc.validSequences().size(); s++){
            states.add(s);
        }

        List<List<Integer>> solutions = new ArrayList<>();

        for(Integer numberOfPhases = Configuration.rc.minPhases(); numberOfPhases <= Configuration.rc.maxPhases(); numberOfPhases++){
            List<List<Integer>> combinations = new ArrayList<>();
            for(int p = 0; p < numberOfPhases; p++){
                combinations.add(durations);
                combinations.add(states);
            }
            solutions.addAll(cartesianProduct(combinations));
        }

        return solutions;
    }

    // returns solutions to evaluate, dependent on the duration step size
    private static List<List<Integer>> removeUnwantedSolutions(List<List<Integer>> solutions) {
        List<List<Integer>> wantedSolutions = new ArrayList<>();
        for(List<Integer> solution : solutions){
            boolean wanted = true;
            for(int i = 0; i < solution.size() && wanted; i = i + 2){
                if(solution.get(i) % durationStepSize > 0){
                    wanted = false;
                }
            }
            if(wanted){
                wantedSolutions.add(solution);
            }
        }
        return wantedSolutions;
    }

    // Removes solutions with consecutive states
    public static List<List<Integer>> removeInvalidSolutions(List<List<Integer>> solutions) {
        List<List<Integer>> validSolutions = new ArrayList<>();
        for(List<Integer> solution : solutions){
            boolean valid = true;
            Integer prevState = -1;
            for(int i = 0; i < solution.size() && valid; i = i + 2){
                if(solution.get(i+1).equals(prevState)){
                    valid = false;
                }
                prevState = solution.get(i+1);
            }
            if(valid){
                validSolutions.add(solution);
            }
        }
        return validSolutions;
    }

    // transforms lists into chromosomes
    private static List<Chromosome> buildChromosomes(List<List<Integer>> solutions){
        List<Chromosome> chromosomes = new ArrayList<>();
        int id = 0;
        for(List<Integer> solution : solutions){
            List<Integer> durations = new ArrayList<>();
            List<Integer> states = new ArrayList<>();
            for(int i = 0; i < solution.size(); i = i + 2){
                durations.add(solution.get(i));
                states.add(solution.get(i+1));
            }
            Chromosome chromosome = new Chromosome(Integer.toString(id), states, durations);
            chromosomes.add(chromosome);
            id++;
        }
        return chromosomes;
    }

    // Build chromosomes and match with list of possible solutions
    private static List<Chromosome> buildChromosomesAndMatch(List<List<Integer>> solutions, List<Chromosome> validChromosomes){
        List<Chromosome> chromosomes = new ArrayList<>();
        for(List<Integer> solution : solutions){
            List<Integer> durations = new ArrayList<>();
            List<Integer> states = new ArrayList<>();
            for(int i = 0; i < solution.size(); i = i + 2){
                durations.add(solution.get(i));
                states.add(solution.get(i+1));
            }
            String ident = "";
            for(Chromosome chromosome : validChromosomes){
                if(chromosome.getSignals().equals(states) && chromosome.getDurations().equals(durations)){
                    ident = chromosome.getId();
                    break;
                }
            }
            if(ident.equals("")){
                ident = UUID.randomUUID().toString();
            }
            Chromosome chromosome = new Chromosome(ident, states, durations);
            chromosomes.add(chromosome);
        }
        return chromosomes;
    }

    private static void printSolutions(List<Chromosome> allChromosomes, List<Chromosome> wantedChromosomes, ConcurrentLinkedQueue<Chromosome> evaluatedChromosomes) {
        StatisticsService.deleteFile(resultsFile);
        StatisticsService.writeTxt(resultsFile, "Fitness;Time-VIP;Time-All;Duration-Sum;Signal-Index;Durations;Signals\n");
        for(Chromosome chromosome : allChromosomes){
            boolean evaled = false;
            for(Chromosome evaluated : evaluatedChromosomes){
                if(evaluated.getId().equals(chromosome.getId())){
                    Double f = evaluated.getFitness().getValue();
                    if(f >= 1000000.0){
                        f = 0.0;
                        System.out.println("Double was max value");
                    }
                    String str = f + ";" + evaluated.getFitness().getTimeForTargetVehicle() + ";" + evaluated.getFitness().getTimeForAllVehicles() + ";" + evaluated.getDurations().stream().mapToInt(Integer::intValue).sum() + ";" + getSignalIndex(evaluated) + ";" + evaluated.getDurations().toString() + ";" + evaluated.getSignals().toString() + "\n";
                    StatisticsService.writeTxt(resultsFile, str);
                    evaled = true;
                    break;
                }
            }
            if(!evaled){
                boolean wanted = false;
                for(Chromosome wantedChromosome : wantedChromosomes){
                    if(wantedChromosome.getId().equals(chromosome.getId())){
                        wanted = true;
                        break;
                    }
                }
                // If a chromosome was not evaluated, it is invalid. If it was skipped it is not in wanted
                // Therefore, if it was not evaluated and is not in wanted, there are no further information -> blindspot
                // If it was not evaluated but it is in wanted, it must be invalid
                if(wanted){
                    String str = "-1;-1;-1;" + chromosome.getDurations().stream().mapToInt(Integer::intValue).sum() + ";" + getSignalIndex(chromosome) + ";" + chromosome.getDurations().toString() + ";" + chromosome.getSignals().toString() + "\n";
                    StatisticsService.writeTxt(resultsFile, str);
                }else{
                    String str = "-2;-2;-2;" + chromosome.getDurations().stream().mapToInt(Integer::intValue).sum() + ";" + getSignalIndex(chromosome) + ";" + chromosome.getDurations().toString() + ";" + chromosome.getSignals().toString() + "\n";
                    StatisticsService.writeTxt(resultsFile, str);
                }
            }
        }
    }

    static int getSignalIndex(Chromosome chromosome){
        if(chromosome.getSignals().equals(Arrays.asList(0,1))){
            return 1;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0))){
            return 2;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1))){
            return 3;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0))){
            return 4;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,1,0))){
            return 5;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0,1))){
            return 6;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,1,1))){
            return 7;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0,1))){
            return 8;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1,0))){
            return 9;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0,0))){
            return 10;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0,0))){
            return 11;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1,1))){
            return 12;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,1,0,1))){
            return 13;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0,1,0))){
            return 14;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0,0,1))){
            return 15;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0,1,1))){
            return 16;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1,1,1))){
            return 17;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1,1,0))){
            return 18;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1,0,0))){
            return 19;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0,0,0))){
            return 20;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,1,0,1))){
            return 21;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0,1,0))){
            return 22;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0,1,1))){
            return 23;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,1,1,1))){
            return 24;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,1,0,0))){
            return 25;
        }else if(chromosome.getSignals().equals(Arrays.asList(1,0,0,0))){
            return 26;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,0,0,1))){
            return 27;
        }else if(chromosome.getSignals().equals(Arrays.asList(0,1,1,0))){
            return 28;
        }
        return -1;
    }

    // Source: https://stackoverflow.com/a/9496234
    private static <T> List<List<T>> cartesianProduct(List<List<T>> lists) {
        List<List<T>> resultLists = new ArrayList<List<T>>();
        if (lists.size() == 0) {
            resultLists.add(new ArrayList<T>());
            return resultLists;
        } else {
            List<T> firstList = lists.get(0);
            List<List<T>> remainingLists = cartesianProduct(lists.subList(1, lists.size()));
            for (T condition : firstList) {
                for (List<T> remainingList : remainingLists) {
                    ArrayList<T> resultList = new ArrayList<T>();
                    resultList.add(condition);
                    resultList.addAll(remainingList);
                    resultLists.add(resultList);
                }
            }
        }
        return resultLists;
    }

}
