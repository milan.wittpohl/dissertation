package common.service;

import DTO.Result;
import common.AbstractPopulation;
import common.data.Chromosome;
import EA.data.Population;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;

// Responsible for various statistics when evaluating a given algorithm multiple times
public class StatisticsService {

    private static final Logger logger = LogManager.getLogger(StatisticsService.class.getName());

    public static Result result;

    public static Integer currentGeneration = 0;
    private static Map<Integer, List<Double>> averageFitnessPerGeneration = new HashMap<>();
    private static Map<Integer, List<Double>> averageDistancesPerGeneration = new HashMap<>();

    public static void addGenerationForAvgFitness(Integer generation, Double bestFitness){
        if(averageFitnessPerGeneration.containsKey(generation)){
            Double newCount = averageFitnessPerGeneration.get(generation).get(0) + 1;
            Double newValue = averageFitnessPerGeneration.get(generation).get(1) + bestFitness;
            averageFitnessPerGeneration.put(generation, Arrays.asList(newCount, newValue));
        }else{
            averageFitnessPerGeneration.put(generation, Arrays.asList(1.0, bestFitness));
        }
    }

    static List<Double> getAverageFitnesses(){
        List<Double> avg = new ArrayList<>();
        for(int i = 0; i <= Configuration.ac.generations(); i++){
            avg.add(averageFitnessPerGeneration.get(i).get(1) / averageFitnessPerGeneration.get(i).get(0));
        }
        return avg;
    }

    static void addRemainingForAvgFitness(){
        for(int i = currentGeneration; i <= Configuration.ac.generations(); i++){
            try{
                addGenerationForAvgFitness(i, result.getBestOverall().getFitness().getValueRegardlessOfSharedFitness());
            }catch(NullPointerException e){
                logger.error("Could not add remaining for generation: " + i);
            }
        }
    }

    public static void addGenerationForAvgDistance(Integer generation, AbstractPopulation population){
        Double avgDistance = 0.0;
        for(Chromosome chromosome : population.getChromosomes()){
            for(Chromosome chromosome1: population.getChromosomes()){
                avgDistance = avgDistance + ChromosomeComparisonService.calculateEuclideanDistance(chromosome, chromosome1);
            }
        }
        avgDistance = avgDistance / (Math.pow(population.getChromosomes().size(), 2));
        if(averageDistancesPerGeneration.containsKey(generation)){
            Double newCount = averageDistancesPerGeneration.get(generation).get(0) + 1;
            Double newValue = averageDistancesPerGeneration.get(generation).get(1) + avgDistance;
            averageDistancesPerGeneration.put(generation, Arrays.asList(newCount, newValue));
        }else{
            averageDistancesPerGeneration.put(generation, Arrays.asList(1.0, avgDistance));
        }
    }

    static List<Double> getAverageDistance(){
        List<Double> avg = new ArrayList<>();
        for(int i = 0; i <= Configuration.ac.generations(); i++){
            avg.add(averageDistancesPerGeneration.get(i).get(1) / averageDistancesPerGeneration.get(i).get(0));
        }
        return avg;
    }

    public static void addRemainingForAvgDistance(){
        for(int i = currentGeneration; i <= Configuration.ac.generations(); i++){
            addGenerationForAvgDistance(i, result.getFinalPopulation());
        }
    }

    public static void deleteFile(String fileName){
        File file = new File(fileName);
        file.delete();
    }

    public static void writeTxt(String filename, String txt){
        try {
            File file = new File(filename);
            file.createNewFile();
            Files.write(file.toPath(), txt.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

}
