package configuration.algorithm.proofOfConcept;

import EA.diversityMechanisms.DefaultDiversityMechanism;
import EA.diversityMechanisms.DiversityMechanism;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.crossover.TwoPointCrossover;
import EA.operators.mutation.MutationOperator;
import EA.operators.mutation.OffspringMutation;
import EA.operators.mutation.SwapMutation;
import EA.operators.replacement.RandomChildReplacement;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.selection.RestrictedByDistanceSelection;
import EA.operators.selection.RestrictedBySpeciesSelection;
import EA.operators.selection.SelectionOperator;
import configuration.algorithm.AlgorithmConfiguration;

import java.util.HashMap;
import java.util.Map;

public final class ProofOfConceptAlgorithm implements AlgorithmConfiguration {

    @Override
    public Boolean multiThreading() {
        return true;
    }

    @Override
    public Integer populationSize() {
        return 10;
    }

    @Override
    public boolean executeMultipleEvaluations() {
        return false;
    }

    @Override
    public boolean sharedFitness() {
        return true;
    }

    @Override
    public double sharedFitnessRadius() {
        return 10;
    }

    @Override
    public DiversityMechanism diversityMechanism() {
        return new DefaultDiversityMechanism();
    }

    @Override
    public Map<String, Object> diversityMechanismOptions() {
        Map options = new HashMap();
        options.put("infusionNumberOfChildren", 5);
        options.put("sawToothNumberOfChildren", 5);
        options.put("sawToothGenerations", 3);
        options.put("sawToothNumberOfChromosomesToRemove", 2);
        options.put("numberOfSpecies", 4);
        return options;
    }

    @Override
    public Map<String, Object> selectionOptions() {
        Map<String, Object> options = new HashMap();
        options.put("tournamentSize", 4);
        options.put("scalingFactor", 2.0);
        options.put("restrictedMatingMaxDistance", 10);
        return options;
    }

    @Override
    public Integer numberOfChildren() {
        return 2;
    }

    @Override
    public Integer generations() {
        return 100;
    }

    @Override
    public SelectionOperator selectionOperator() {
        return new RestrictedBySpeciesSelection();
    }

    @Override
    public CrossoverOperator crossoverOperatorDuration() {
        return new TwoPointCrossover();
    }

    @Override
    public CrossoverOperator crossoverOperatorSignals() {
        return new TwoPointCrossover();
    }

    @Override
    public Double mutationProbability() {
        return 0.5;
    }

    @Override
    public MutationOperator mutationOperatorStates() {
        return new SwapMutation();
    }

    @Override
    public MutationOperator mutationOperatorDurations() {
        return new OffspringMutation();
    }

    @Override
    public ReplacementOperator replacementOperator() {
        return new RandomChildReplacement();
    }
}
