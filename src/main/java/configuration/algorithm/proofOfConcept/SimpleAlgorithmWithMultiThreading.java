package configuration.algorithm.proofOfConcept;

import EA.diversityMechanisms.DefaultDiversityMechanism;
import EA.diversityMechanisms.DiversityMechanism;
import EA.operators.crossover.OnePointCrossover;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.mutation.MutationOperator;
import EA.operators.mutation.SwapMutation;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.replacement.WorstChildReplacement;
import EA.operators.selection.SelectionOperator;
import EA.operators.selection.TournamentSelection;
import configuration.algorithm.AlgorithmConfiguration;

import java.util.HashMap;
import java.util.Map;

public final class SimpleAlgorithmWithMultiThreading extends SimpleAlgorithm {

    @Override
    public Boolean multiThreading() {
        return true;
    }
}
