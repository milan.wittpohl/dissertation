package configuration.algorithm.problemScenarios.finalised;

import EA.diversityMechanisms.DiversityMechanism;
import EA.diversityMechanisms.InfusionDiversityMechanism;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.crossover.TwoPointCrossover;
import EA.operators.mutation.MutationOperator;
import EA.operators.mutation.OffspringMutation;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.replacement.WorstChildWithProbabilityReplacement;
import EA.operators.selection.RestrictedByDistanceSelection;
import EA.operators.selection.SelectionOperator;
import configuration.algorithm.AlgorithmConfiguration;

import java.util.HashMap;
import java.util.Map;

public final class ExploreByMaintainingDistance_Algorithm implements AlgorithmConfiguration {

    @Override
    public Boolean multiThreading() {
        return true;
    }

    @Override
    public Integer populationSize() {
        return 50;
    }

    @Override
    public boolean executeMultipleEvaluations() {
        return false;
    }

    @Override
    public boolean sharedFitness() {
        return false;
    }

    @Override
    public double sharedFitnessRadius() {
        return 0;
    }

    @Override
    public DiversityMechanism diversityMechanism() {
        return new InfusionDiversityMechanism();
    }

    @Override
    public Map<String, Object> diversityMechanismOptions() {
        Map options = new HashMap();
        options.put("infusionNumberOfChildren", 2);
        return options;
    }

    @Override
    public Map<String, Object> selectionOptions() {
        Map options = new HashMap();
        options.put("tournamentSize", 5);
        options.put("restrictedMatingMaxDistance", 50);
        return options;
    }

    @Override
    public Integer numberOfChildren() {
        return 10;
    }

    @Override
    public Integer generations() {
        return 100;
    }

    @Override
    public SelectionOperator selectionOperator() {
        return new RestrictedByDistanceSelection();
    }

    @Override
    public CrossoverOperator crossoverOperatorDuration() {
        return new TwoPointCrossover();
    }

    @Override
    public CrossoverOperator crossoverOperatorSignals() {
        return new TwoPointCrossover();
    }

    @Override
    public Double mutationProbability() {
        return 0.5;
    }

    @Override
    public MutationOperator mutationOperatorStates() {
        return new OffspringMutation();
    }

    @Override
    public MutationOperator mutationOperatorDurations() {
        return new OffspringMutation();
    }

    @Override
    public ReplacementOperator replacementOperator() {
        return new WorstChildWithProbabilityReplacement();
    }
}
