package configuration.algorithm.problemScenarios.revised;

import EA.diversityMechanisms.DiversityMechanism;
import EA.diversityMechanisms.SawToothDiversityMechanism;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.crossover.UniformCrossover;
import EA.operators.mutation.MutationOperator;
import EA.operators.mutation.ShuffleMutation;
import EA.operators.replacement.OldestChildReplacement;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.selection.RankingSelection;
import EA.operators.selection.SelectionOperator;
import configuration.algorithm.AlgorithmConfiguration;

import java.util.HashMap;
import java.util.Map;

public final class ProblemScenario_VI_V_Algorithm implements AlgorithmConfiguration {

    @Override
    public Boolean multiThreading() {
        return true;
    }

    @Override
    public Integer populationSize() {
        return 50;
    }

    @Override
    public boolean executeMultipleEvaluations() {
        return false;
    }

    @Override
    public boolean sharedFitness() {
        return true;
    }

    @Override
    public double sharedFitnessRadius() {
        return 10;
    }

    @Override
    public DiversityMechanism diversityMechanism() {
        return new SawToothDiversityMechanism();
    }

    @Override
    public Map<String, Object> diversityMechanismOptions() {
        Map options = new HashMap();
        options.put("sawToothNumberOfChildren", 25);
        options.put("sawToothGenerations", 5);
        options.put("sawToothNumberOfChromosomesToRemove", 5);
        return options;
    }

    @Override
    public Map<String, Object> selectionOptions() {
        Map options = new HashMap();
        options.put("scalingFactor", 1.1);
        return options;
    }

    @Override
    public Integer numberOfChildren() {
        return 10;
    }

    @Override
    public Integer generations() {
        return 100;
    }

    @Override
    public SelectionOperator selectionOperator() {
        return new RankingSelection();
    }

    @Override
    public CrossoverOperator crossoverOperatorDuration() {
        return new UniformCrossover();
    }

    @Override
    public CrossoverOperator crossoverOperatorSignals() {
        return new UniformCrossover();
    }

    @Override
    public Double mutationProbability() {
        return 0.5;
    }

    @Override
    public MutationOperator mutationOperatorStates() {
        return new ShuffleMutation();
    }

    @Override
    public MutationOperator mutationOperatorDurations() {
        return new ShuffleMutation();
    }

    @Override
    public ReplacementOperator replacementOperator() {
        return new OldestChildReplacement();
    }
}
