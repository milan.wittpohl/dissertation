package configuration.algorithm;

import PSO.Particle;

/**
 * The PSO algorithm configuration extends the general configuration by adding a PSO config
 */
public interface PSOAlgorithmConfiguration extends AlgorithmConfiguration {

    public Particle.PsoConfig psoConfig();

}
