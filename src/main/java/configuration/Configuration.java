package configuration;

import configuration.algorithm.AlgorithmConfiguration;
import configuration.algorithm.PSO.PSO_Active_Algorithm;
import configuration.algorithm.PSO.PSO_Calm_Algorithm;
import configuration.algorithm.problemScenarios.ProblemScenarioIAlgorithm;
import configuration.algorithm.problemScenarios.finalised.*;
import configuration.algorithm.problemScenarios.initial.ProblemScenario_I_II_II_Algorithm;
import configuration.algorithm.problemScenarios.initial.ProblemScenario_I_II_I_Algorithm;
import configuration.algorithm.problemScenarios.initial.ProblemScenario_I_I_II_Algorithm;
import configuration.algorithm.problemScenarios.initial.ProblemScenario_I_I_I_Algorithm;
import configuration.algorithm.problemScenarios.revised.*;
import configuration.algorithm.proofOfConcept.ProofOfConceptAlgorithm;
import configuration.algorithm.proofOfConcept.SimpleAlgorithm;
import configuration.algorithm.proofOfConcept.SimpleAlgorithmWithMultiThreading;
import configuration.run.PSO.PSO_I_Run;
import configuration.run.RunConfiguration;
import configuration.run.problemScenarios.ProblemScenarioIRun;
import configuration.run.problemScenarios.finalised.CreativeWithLocalSearch_Run;
import configuration.run.problemScenarios.finalised.Creative_Run;
import configuration.run.problemScenarios.finalised.Creative_Run_No_Prioritisation;
import configuration.run.problemScenarios.finalised.Creative_Run_Reduced_Time;
import configuration.run.problemScenarios.initial.ProblemScenario_I_II_II_Run;
import configuration.run.problemScenarios.initial.ProblemScenario_I_II_I_Run;
import configuration.run.problemScenarios.initial.ProblemScenario_I_I_II_Run;
import configuration.run.problemScenarios.initial.ProblemScenario_I_I_I_Run;
import configuration.run.problemScenarios.revised.*;
import configuration.run.proofOfConcept.ProofOfConceptRun;
import configuration.run.proofOfConcept.ProofOfConceptRun_Unit_Tests;
import configuration.scenario.ScenarioConfiguration;
import configuration.scenario.problemScenarios.ProblemScenarioIScenario;
import configuration.scenario.problemScenarios.finalised.*;
import configuration.scenario.problemScenarios.initial.ProblemScenario_I_II_II_Scenario;
import configuration.scenario.problemScenarios.initial.ProblemScenario_I_II_I_Scenario;
import configuration.scenario.problemScenarios.initial.ProblemScenario_I_I_II_Scenario;
import configuration.scenario.problemScenarios.initial.ProblemScenario_I_I_I_Scenario;
import configuration.scenario.problemScenarios.revised.*;
import configuration.scenario.proofOfConcept.ProofOfConceptPedestriansScenario;
import configuration.scenario.proofOfConcept.ProofOfConceptScenario;
import configuration.scenario.proofOfConcept.SimpleRealWorldScenario;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Configuration class for an EA/PSO
 * A configuration consists of a algorithm, run and scenario configuration
 * Additional configurations can be defined in this class
 * The identifier is used as argument when launching the application (see readme in root folder)
 */
public final class Configuration {

    private static final Logger logger = LogManager.getLogger(Configuration.class.getName());

    @Getter
    private static String identifier;

    public static AlgorithmConfiguration ac;
    public static RunConfiguration rc;
    public static ScenarioConfiguration sc;

    public Configuration(String scenario){
        identifier = scenario;
        setConfig(scenario);
        logger.info("Running with configuration: " + scenario);
    }

    public static void setConfig(String config){
        identifier = config;
        switch (config){
            case("POC"):
                ac = new ProofOfConceptAlgorithm();
                rc = new ProofOfConceptRun();
                sc = new ProofOfConceptScenario();
                break;
            case("POC-PED"):
                ac = new SimpleAlgorithm();
                rc = new ProofOfConceptRun();
                sc = new ProofOfConceptPedestriansScenario();
                break;
            case("596"):
                ac = new SimpleAlgorithmWithMultiThreading();
                rc = new ProofOfConceptRun();
                sc = new SimpleRealWorldScenario();
                break;
            case("PS-1"):
                ac = new ProblemScenarioIAlgorithm();
                rc = new ProblemScenarioIRun();
                sc = new ProblemScenarioIScenario();
                break;
            case("PS-I-I-I"):
                ac = new ProblemScenario_I_I_I_Algorithm();
                rc = new ProblemScenario_I_I_I_Run();
                sc = new ProblemScenario_I_I_I_Scenario();
                break;
            case("PS-I-I-II"):
                ac = new ProblemScenario_I_I_II_Algorithm();
                rc = new ProblemScenario_I_I_II_Run();
                sc = new ProblemScenario_I_I_II_Scenario();
                break;
            case("PS-I-II-I"):
                ac = new ProblemScenario_I_II_I_Algorithm();
                rc = new ProblemScenario_I_II_I_Run();
                sc = new ProblemScenario_I_II_I_Scenario();
                break;
            case("PS-I-II-II"):
                ac = new ProblemScenario_I_II_II_Algorithm();
                rc = new ProblemScenario_I_II_II_Run();
                sc = new ProblemScenario_I_II_II_Scenario();
                break;
            case("PS-II-I"):
                ac = new ProblemScenario_II_I_Algorithm();
                rc = new ProblemScenario_II_I_Run();
                sc = new ProblemScenario_II_I_Scenario();
                break;
            case("PS-II-II"):
                ac = new ProblemScenario_II_II_Algorithm();
                rc = new ProblemScenario_II_II_Run();
                sc = new ProblemScenario_II_II_Scenario();
                break;
            case("PS-III-I"):
                ac = new ProblemScenario_III_I_Algorithm();
                rc = new ProblemScenario_III_I_Run();
                sc = new ProblemScenario_III_I_Scenario();
                break;
            case("PS-III-II"):
                ac = new ProblemScenario_III_II_Algorithm();
                rc = new ProblemScenario_III_II_Run();
                sc = new ProblemScenario_III_II_Scenario();
                break;
            case("PS-IV"):
                ac = new ProblemScenario_IV_Algorithm();
                rc = new ProblemScenario_IV_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-IV-I"):
                ac = new ProblemScenario_IV_I_Algorithm();
                rc = new ProblemScenario_IV_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-IV-II"):
                ac = new ProblemScenario_IV_II_Algorithm();
                rc = new ProblemScenario_IV_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-IV-III"):
                ac = new ProblemScenario_IV_III_Algorithm();
                rc = new ProblemScenario_IV_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-V"):
                ac = new ProblemScenario_IV_Algorithm();
                rc = new ProblemScenario_V_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-V-I"):
                ac = new ProblemScenario_IV_I_Algorithm();
                rc = new ProblemScenario_V_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-V-II"):
                ac = new ProblemScenario_IV_II_Algorithm();
                rc = new ProblemScenario_V_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-V-III"):
                ac = new ProblemScenario_IV_III_Algorithm();
                rc = new ProblemScenario_V_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VI-I"):
                ac = new ProblemScenario_VI_I_Algorithm();
                rc = new ProblemScenario_VI_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VI-II"):
                ac = new ProblemScenario_VI_II_Algorithm();
                rc = new ProblemScenario_VI_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VI-III"):
                ac = new ProblemScenario_VI_III_Algorithm();
                rc = new ProblemScenario_VI_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VI-IV"):
                ac = new ProblemScenario_VI_IV_Algorithm();
                rc = new ProblemScenario_VI_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VI-V"):
                ac = new ProblemScenario_VI_V_Algorithm();
                rc = new ProblemScenario_VI_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-I"):
                ac = new ProblemScenario_VII_I_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-I-I"):
                ac = new ProblemScenario_VII_I_I_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-II"):
                ac = new ProblemScenario_VII_II_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-II-I"):
                ac = new ProblemScenario_VII_II_I_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-III"):
                ac = new ProblemScenario_VII_III_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-III-I"):
                ac = new ProblemScenario_VII_II_I_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-IV"):
                ac = new ProblemScenario_VII_IV_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VII-IV-I"):
                ac = new ProblemScenario_VII_II_I_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-VIII"):
                ac = new ProblemScenario_VIII_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-IX"):
                ac = new ProblemScenario_IX_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-IX-I"):
                ac = new ProblemScenario_IX_I_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PS-X"):
                ac = new ProblemScenario_X_Algorithm();
                rc = new ProblemScenario_VII_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("PSO-I"):
                ac = new PSO_Calm_Algorithm();
                rc = new PSO_I_Run();
                sc = new ProblemScenario_IV_Scenario();
                break;
            case("C-BASE-FC"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-EX"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-FC-LS"):
                ac = new FastConvergence_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-EX-LS"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-PSOC"):
                ac = new PSO_Calm_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-PSOA"):
                ac = new PSO_Active_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-EW-FC"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_Scenario();
                break;
            case("C-EW-EX"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_Scenario();
                break;
            case("C-EW-FC-LS"):
                ac = new FastConvergence_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_east_west_Scenario();
                break;
            case("C-EW-EX-LS"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_east_west_Scenario();
                break;
            case("C-EW-PSOC"):
                ac = new PSO_Calm_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_Scenario();
                break;
            case("C-EW-PSOA"):
                ac = new PSO_Active_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_Scenario();
                break;
            case("C-NS-FC"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_north_south_Scenario();
                break;
            case("C-NS-EX"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_north_south_Scenario();
                break;
            case("C-NS-FC-LS"):
                ac = new FastConvergence_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_north_south_Scenario();
                break;
            case("C-NS-EX-LS"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_north_south_Scenario();
                break;
            case("C-NS-PSOC"):
                ac = new PSO_Calm_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_north_south_Scenario();
                break;
            case("C-NS-PSOA"):
                ac = new PSO_Active_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_north_south_Scenario();
                break;
            case("C-EWS-FC"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_south_Scenario();
                break;
            case("C-EWS-EX"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_south_Scenario();
                break;
            case("C-EWS-FC-LS"):
                ac = new FastConvergence_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_east_west_south_Scenario();
                break;
            case("C-EWS-EX-LS"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_east_west_south_Scenario();
                break;
            case("C-EWS-PSOC"):
                ac = new PSO_Calm_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_south_Scenario();
                break;
            case("C-EWS-PSOA"):
                ac = new PSO_Active_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_south_Scenario();
                break;
            case("C-EWNS-FC"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-EX"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-FC-LS"):
                ac = new FastConvergence_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-EX-LS"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new CreativeWithLocalSearch_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-PSOC"):
                ac = new PSO_Calm_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-PSOA"):
                ac = new PSO_Active_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-BASE-FC-NP"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run_No_Prioritisation();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-EX-NP"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run_No_Prioritisation();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-EWNS-FC-NP"):
                ac = new FastConvergence_Algorithm();
                rc = new Creative_Run_No_Prioritisation();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-EX-NP"):
                ac = new ExploreByMaintainingDistance_Algorithm();
                rc = new Creative_Run_No_Prioritisation();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-BASE-FC-ME"):
                ac = new FastConvergence_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-BASE-EX-ME"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_Scenario();
                break;
            case("C-EWNS-FC-ME"):
                ac = new FastConvergence_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-EWNS-EX-ME"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_Scenario();
                break;
            case("C-BASE-FC-ME-RE"):
                ac = new FastConvergence_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_robustness_Scenario();
                break;
            case("C-BASE-EX-ME-RE"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_robustness_Scenario();
                break;
            case("C-BASE-PED-FC-ME"):
                ac = new FastConvergence_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_pedestrian_Scenario();
                break;
            case("C-BASE-PED-EX-ME"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_base_pedestrian_Scenario();
                break;
            case("C-EWNS-PED-FC-ME"):
                ac = new FastConvergence_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_pedestrians_Scenario();
                break;
            case("C-EWNS-PED-EX-ME"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Algorithm();
                rc = new Creative_Run();
                sc = new RealWorld_Hamburg_596_east_west_north_south_pedestrians_Scenario();
                break;
            case("C-BASE-PED-FC-ME-RT"):
                ac = new FastConvergence_MultipleExecutions_Reduced_Population_Algorithm();
                rc = new Creative_Run_Reduced_Time();
                sc = new RealWorld_Hamburg_596_base_pedestrian_Scenario();
                break;
            case("C-BASE-PED-EX-ME-RT"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Recued_Population_Algorithm();
                rc = new Creative_Run_Reduced_Time();
                sc = new RealWorld_Hamburg_596_base_pedestrian_Scenario();
                break;
            case("C-EWNS-PED-FC-ME-RT"):
                ac = new FastConvergence_MultipleExecutions_Reduced_Population_Algorithm();
                rc = new Creative_Run_Reduced_Time();
                sc = new RealWorld_Hamburg_596_east_west_north_south_pedestrians_Scenario();
                break;
            case("C-EWNS-PED-EX-ME-RT"):
                ac = new ExploreByMaintainingDistance_MultipleExecutions_Recued_Population_Algorithm();
                rc = new Creative_Run_Reduced_Time();
                sc = new RealWorld_Hamburg_596_east_west_north_south_pedestrians_Scenario();
                break;
            case ("UNIT-TESTS"):
                ac = new SimpleAlgorithm();
                rc = new ProofOfConceptRun_Unit_Tests();
                sc = new ProofOfConceptScenario();
                break;
            default:
                ac = new SimpleAlgorithm();
                rc = new ProofOfConceptRun();
                sc = new ProofOfConceptScenario();
        }
    }

}
