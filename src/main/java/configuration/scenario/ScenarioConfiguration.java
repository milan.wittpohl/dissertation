package configuration.scenario;

import java.util.HashMap;
import java.util.List;

/**
 * The scenario configuration consits of
 *  -> the paths to the static SUMO files (network and routes)
 *  -> information for the EA/PSO such as valid states or the weight/importance of the target vehicle
 */
public interface ScenarioConfiguration {

    public String network();
    public String routes();
    public String defaultSignals();
    public String idOfTargetVehicle();
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState);
    public HashMap<String, String> states();
    public List<List<String>> validSequences();
    public Integer maxSimulationTime();

}
