package configuration.scenario.proofOfConcept;

import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SimpleRealWorldScenario extends AbstractScenarioConfiguration {

    private static final Logger logger = LogManager.getLogger(SimpleRealWorldScenario.class.getName());

    @Override
    public String network() {
        return "596/network.net.xml";
    }

    @Override
    public String routes() {
        return "596/routes.rou.xml";
    }

    @Override
    public String defaultSignals() {
        return "596/signals-default.sig.xml";
    }

    @Override
    public String idOfTargetVehicle() {
        return "VIP";
    }

    @Override
    public HashMap<String, String> states(){
        HashMap<String, String> states = new HashMap<>();

        states.put("ALL_RED", "rrrrrrrrrrrrrrr");

        states.put("EAST_WEST_YELLOW_GREEN", "rrruuuurrruuuuu");
        states.put("EAST_WEST_GREEN", "rrrgGGgrrrgGGgg");
        states.put("EAST_WEST_YELLOW_RED", "rrryyyyrrryyyyy");

        states.put("EAST_WEST_LEFT_YELLOW_GREEN", "rrrurrurrrurruu");
        states.put("EAST_WEST_LEFT_GREEN", "rrrGrrGrrrGrrGG");
        states.put("EAST_WEST_LEFT_YELLOW_RED", "rrryrryrrryrryy");

        states.put("NORTH_SOUTH_YELLOW_GREEN", "uuurrrruuurrrrr");
        states.put("NORTH_SOUTH_GREEN", "gGgrrrrgGgrrrrr");
        states.put("NORTH_SOUTH_YELLOW_RED", "yyyrrrryyyrrrrr");

        states.put("NORTH_SOUTH_LEFT_YELLOW_GREEN", "ururrrrururrrrr");
        states.put("NORTH_SOUTH_LEFT_GREEN", "GrGrrrrGrGrrrrr");
        states.put("NORTH_SOUTH_LEFT_YELLOW_RED", "yryrrrryryrrrrr");

        return states;
    }

    @Override
    public List<List<String>> validSequences() {
        HashMap<String, String> states = states();
        List<String> validSequenceA = Arrays.asList("EAST_WEST_YELLOW_GREEN", "EAST_WEST_GREEN", "EAST_WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceB = Arrays.asList("EAST_WEST_LEFT_YELLOW_GREEN", "EAST_WEST_LEFT_GREEN", "EAST_WEST_LEFT_YELLOW_RED", "ALL_RED");
        List<String> validSequenceC = Arrays.asList("NORTH_SOUTH_YELLOW_GREEN", "NORTH_SOUTH_GREEN", "NORTH_SOUTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceD = Arrays.asList("NORTH_SOUTH_LEFT_YELLOW_GREEN", "NORTH_SOUTH_LEFT_GREEN", "NORTH_SOUTH_LEFT_YELLOW_RED", "ALL_RED");

        List<List<String>> sequences = Arrays.asList(validSequenceA, validSequenceB, validSequenceC, validSequenceD);
        return transformSequences(states, sequences);
    }

    @Override
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState){
        switch (sourceState){
            case 1:
            case 2:
                switch (targetState){
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 3, 3);
                }
            case 3:
            case 4:
                switch (targetState){
                    case 1:
                    case 2:
                        return Arrays.asList(1, -1, 3, 5);
                }
            case 0:
                switch (targetState){
                    case 1:
                    case 2:
                        return Arrays.asList(1, -1, 3, 5);
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 3, 3);
                }
        }

        logger.error("No valid intergreen times for: " + sourceState + " - " + targetState);
        return null;
    }

    @Override
    public Integer maxSimulationTime() {
        return 500;
    }
}
