package configuration.run;

/**
 * The run configuration consists of values for each run (e.g. the number of phases)
 */
public interface RunConfiguration {

    public Integer minPhases();
    public Integer maxPhases();
    public Integer minPhaseDuration();
    public Integer maxPhaseDuration();
    public Double importanceOfTargetVehicle();
    public long timeLimit();
    public long timeLimitLocalSearch();
}
