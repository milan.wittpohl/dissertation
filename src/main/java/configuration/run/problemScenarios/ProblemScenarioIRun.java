package configuration.run.problemScenarios;

import configuration.run.RunConfiguration;

public class ProblemScenarioIRun implements RunConfiguration {

    @Override
    public Integer minPhases() {
        return 2;
    }

    @Override
    public Integer maxPhases() {
        return 4;
    }

    @Override
    public Integer minPhaseDuration() {
        return 5;
    }

    @Override
    public Integer maxPhaseDuration() {
        return 60;
    }

    @Override
    public Double importanceOfTargetVehicle() {
        return 0.7;
    }

    @Override
    public long timeLimit() {
        return 200000;
    }

    @Override
    public long timeLimitLocalSearch() {
        return 0;
    }
}
