package configuration.run.problemScenarios.initial;

import configuration.run.RunConfiguration;

public class ProblemScenario_I_I_I_Run implements RunConfiguration {

    @Override
    public Integer minPhases() {
        return 2;
    }

    @Override
    public Integer maxPhases() {
        return 4;
    }

    @Override
    public Integer minPhaseDuration() {
        return 5;
    }

    @Override
    public Integer maxPhaseDuration() {
        return 30;
    }

    @Override
    public Double importanceOfTargetVehicle() {
        return 0.7;
    }

    @Override
    public long timeLimit() {
        return 20000;
    }

    @Override
    public long timeLimitLocalSearch() {
        return 0;
    }
}
