package configuration.run.proofOfConcept;

import configuration.run.RunConfiguration;

public class ProofOfConceptRun_Unit_Tests implements RunConfiguration {

    @Override
    public Integer minPhases() {
        return 4;
    }

    @Override
    public Integer maxPhases() {
        return 4;
    }

    @Override
    public Integer minPhaseDuration() {
        return 1;
    }

    @Override
    public Integer maxPhaseDuration() {
        return 10;
    }

    @Override
    public Double importanceOfTargetVehicle() {
        return 0.7;
    }

    @Override
    public long timeLimit() {
        return 120000;
    }

    @Override
    public long timeLimitLocalSearch() {
        return 0;
    }
}
