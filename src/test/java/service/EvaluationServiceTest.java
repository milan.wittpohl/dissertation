package service;

import DTO.*;
import common.service.CleanUpService;
import common.service.EvaluationService;
import common.service.StatisticsService;
import configuration.Configuration;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

class EvaluationServiceTest {

    private static String parent = Paths.get(".").toAbsolutePath().normalize().toString();

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("UNIT-TESTS");
        try {
            Files.copy(Paths.get(parent + "/scenarios/tests/test-errors.out.txt"), Paths.get(parent + "/scenarios/tmp/errors/test-errors.out.txt"), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(Paths.get(parent + "/scenarios/tests/test-trips.out.xml"), Paths.get(parent + "/scenarios/tmp/trips/test-trips.out.xml"), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(Paths.get(parent + "/scenarios/tests/test-2-trips.out.xml"), Paths.get(parent + "/scenarios/tmp/trips/test-2-trips.out.xml"), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(Paths.get(parent + "/scenarios/tests/test-aborted.out.xml"), Paths.get(parent + "/scenarios/tmp/trips/test-aborted-trips.out.xml"), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(Paths.get(parent + "/scenarios/tests/test-aborted-2.out.xml"), Paths.get(parent + "/scenarios/tmp/trips/test-aborted-2-trips.out.xml"), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeEach
    void setUp(){
        StatisticsService.result = new Result();
    }

    @AfterAll
    static void cleanUp(){
        CleanUpService.cleanUp();
    }

    // JOURNEY TIMES

    @Test
    void calculateJourneyTimesPersonLast(){
        SimulationResults simulationResults = EvaluationService.calculateJourneyTimes(parent, "TEST");
        Assertions.assertEquals(simulationResults.getTimeForTargetVehicle(), new Double(104));
        Assertions.assertEquals(simulationResults.getTimeForAllVehicles(), new Double(436));
        Assertions.assertFalse(simulationResults.getWasAborted());
    }

    @Test
    void calculateJourneyTimesVehicleLast(){
        SimulationResults simulationResults = EvaluationService.calculateJourneyTimes(parent, "TEST-2");
        Assertions.assertEquals(simulationResults.getTimeForTargetVehicle(), new Double(104));
        Assertions.assertEquals(simulationResults.getTimeForAllVehicles(), new Double(437));
        Assertions.assertFalse(simulationResults.getWasAborted());
    }

    @Test
    void calculateJourneyTimesAborted(){
        SimulationResults simulationResults = EvaluationService.calculateJourneyTimes(parent, "TEST-ABORTED");
        Assertions.assertEquals(simulationResults.getTimeForTargetVehicle(), new Double(201));
        Assertions.assertTrue(simulationResults.getTimeForAllVehicles() == Double.MAX_VALUE);
        Assertions.assertTrue(simulationResults.getWasAborted());
    }

    @Test
    void calculateJourneyTimesAbortedIncludingTargetVehicle(){
        SimulationResults simulationResults = EvaluationService.calculateJourneyTimes(parent, "TEST-ABORTED-2");
        Assertions.assertTrue(simulationResults.getTimeForTargetVehicle() == Double.MAX_VALUE);
        Assertions.assertTrue(simulationResults.getTimeForAllVehicles() == Double.MAX_VALUE);
        Assertions.assertTrue(simulationResults.getWasAborted());
    }

    // Penalties
    @Test
    void testPenaltyExtraction(){
        List<Penalty> penalties = EvaluationService.calculatePenalties(parent, "TEST");
        Assertions.assertEquals(penalties.get(0).getReason(), Penalty.Reason.JAM);
        Assertions.assertEquals(penalties.get(1).getReason(), Penalty.Reason.JAM);
        Assertions.assertEquals(penalties.get(2).getReason(), Penalty.Reason.EMERGENCY_STOP);
        Assertions.assertEquals(penalties.get(3).getReason(), Penalty.Reason.JAM);
        Assertions.assertEquals(penalties.get(4).getReason(), Penalty.Reason.EMERGENCY_BREAK);
        Assertions.assertEquals(penalties.get(5).getReason(), Penalty.Reason.JAM);
        Assertions.assertEquals(penalties.get(6).getReason(), Penalty.Reason.JAM);
        Assertions.assertEquals(penalties.get(7).getReason(), Penalty.Reason.COLLISION);
        Assertions.assertEquals(penalties.get(8).getReason(), Penalty.Reason.COLLISION);
        Assertions.assertEquals(penalties.get(9).getReason(), Penalty.Reason.EMERGENCY_BREAK);
        Assertions.assertEquals(penalties.get(10).getReason(), Penalty.Reason.COLLISION);
        Assertions.assertEquals(penalties.get(11).getReason(), Penalty.Reason.COLLISION);
        Assertions.assertEquals(penalties.get(12).getReason(), Penalty.Reason.EMERGENCY_STOP);
        Assertions.assertEquals(penalties.get(13).getReason(), Penalty.Reason.COLLISION);
        Assertions.assertEquals(penalties.get(14).getReason(), Penalty.Reason.EMERGENCY_BREAK);
        Assertions.assertEquals(penalties.get(15).getReason(), Penalty.Reason.EMERGENCY_STOP);
    }

    // Fitness Function
    @Test
    void testFitnessFunctionNoPenalties(){
        SimulationResults simulationResults = EvaluationService.calculateJourneyTimes(parent, "TEST");
        Fitness fitness = EvaluationService.calculateFitness(simulationResults);
        Assertions.assertEquals(fitness.getTimeForTargetVehicle(), simulationResults.getTimeForTargetVehicle());
        Assertions.assertEquals(fitness.getTimeForAllVehicles(), simulationResults.getTimeForAllVehicles());
        Assertions.assertTrue(fitness.getTotalPenalty() == 0.0);
        Assertions.assertFalse(fitness.getWasAborted());
        Double correctFitness = fitness.getTimeForTargetVehicle() * Configuration.rc.importanceOfTargetVehicle() + fitness.getTimeForAllVehicles() * (1 - Configuration.rc.importanceOfTargetVehicle());
        Assertions.assertEquals(correctFitness, fitness.getValue());
    }

    @Test
    void testFitnessFunction(){
        SimulationResults simulationResults = EvaluationService.calculateJourneyTimes(parent, "TEST");
        simulationResults.setPenalties(EvaluationService.calculatePenalties(parent, "TEST"));

        Fitness fitness = EvaluationService.calculateFitness(simulationResults);
        Assertions.assertEquals(fitness.getTimeForTargetVehicle(), simulationResults.getTimeForTargetVehicle());
        Assertions.assertEquals(fitness.getTimeForAllVehicles(), simulationResults.getTimeForAllVehicles());
        Assertions.assertEquals(new Double(Math.floor(fitness.getTotalPenalty() * 1000) / 1000), new Double((5 * (203.6 * 0.01)) + (3 * (203.6 * 0.01)) + (3 * (203.6 * 0.01)) + (5 * (203.6 * 1.00))));
        Assertions.assertFalse(fitness.getWasAborted());
        Double correctFitness = fitness.getTimeForTargetVehicle() * Configuration.rc.importanceOfTargetVehicle() + fitness.getTimeForAllVehicles() * (1 - Configuration.rc.importanceOfTargetVehicle());
        Assertions.assertEquals(new Double(correctFitness + fitness.getTotalPenalty()), fitness.getValue());
    }

}