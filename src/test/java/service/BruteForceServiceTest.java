package service;

import common.service.BruteForceService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

class BruteForceServiceTest {

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    /*
           -> exactly four phases per cycle
           -> phases can be between 5 and 10 seconds long
           -> four valid phases
    */
    @Test
    void buildSolutions() {
        List<List<Integer>> solutions = BruteForceService.buildSolutions();
        List<List<Integer>> validSolutions = BruteForceService.removeInvalidSolutions(solutions);
        for(List<Integer> validSolution : validSolutions) {
            Assertions.assertTrue(validSolution.size() == Configuration.rc.maxPhases() * 2);
        }
    }


}