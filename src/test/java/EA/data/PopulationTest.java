package EA.data;

import DTO.Result;
import DTO.Fitness;
import common.data.Chromosome;
import common.service.CleanUpService;
import common.service.ParallelisationService;
import common.service.StatisticsService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;

class PopulationTest {

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
        ParallelisationService.generateIds(Configuration.ac.populationSize() + (Configuration.ac.numberOfChildren() * Configuration.ac.generations()));
    }

    @BeforeEach
    void setUp() {
    }

    @Test
    void evaluatePopulation() {
        StatisticsService.result = new Result();
        Population population = new Population();
        Fitness nullFitness = new Fitness(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        population.getChromosomes().forEach(chromosome -> {
            Assertions.assertEquals(chromosome.getFitness().getValue(), nullFitness.getValue());
            Assertions.assertEquals(chromosome.getFitness().getTimeForTargetVehicle(), nullFitness.getTimeForTargetVehicle());
            Assertions.assertEquals(chromosome.getFitness().getTimeForAllVehicles(), nullFitness.getTimeForAllVehicles());
        });
        population.evaluatePopulation();
        population.getChromosomes().forEach(chromosome -> {
            Assertions.assertNotEquals(chromosome.getFitness().getValue(), nullFitness.getValue());
            if(!chromosome.getFitness().getWasAborted()){
                Assertions.assertNotEquals(chromosome.getFitness().getTimeForTargetVehicle(), nullFitness.getTimeForTargetVehicle());
                Assertions.assertNotEquals(chromosome.getFitness().getTimeForAllVehicles(), nullFitness.getTimeForAllVehicles());
            }
        });
        CleanUpService.cleanUp();
    }

    @Test
    void getBest() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Population population = new Population(new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB)));
        Assertions.assertEquals(population.getBest(), chromosomeB);
    }

    @Test
    void getBestSame() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Population population = new Population(new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB)));
        Assertions.assertEquals(population.getBest(), chromosomeA);
    }

    @Test
    void getBestForTargetVehicle() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 1.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Population population = new Population(new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB)));
        Assertions.assertEquals(population.getBestForTargetVehicle(), chromosomeB);
    }

    @Test
    void getBestForAllVehicles() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 1.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Population population = new Population(new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB)));
        Assertions.assertEquals(population.getBestForAllVehicles(), chromosomeB);
    }
}