package EA.operators.crossover;

import DTO.Fitness;
import DTO.Parents;
import common.data.Chromosome;
import common.service.ParallelisationService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class OnePointCrossoverTest {

    OnePointCrossover onePointCrossover;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp(){
        onePointCrossover = new OnePointCrossover();
    }

    @Test
    void crossoverDurationParentALonger() {
        ParallelisationService.generateIds(2);
        Chromosome parentA = new Chromosome("A", new ArrayList<>(), Arrays.asList(1, 2, 3), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome parentB = new Chromosome("B", new ArrayList<>(), Arrays.asList(4, 5), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Parents parents = new Parents(parentA, parentB);
        List<Integer> crossover = onePointCrossover.crossoverDurations(parents);
        for(Integer duration : parentB.getDurations()) {
            Assertions.assertTrue(crossover.contains(duration));
        }
    }

    @Test
    void crossoverDurationParentBLonger() {
        ParallelisationService.generateIds(2);
        Chromosome parentA = new Chromosome("A", new ArrayList<>(), Arrays.asList(1, 2), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome parentB = new Chromosome("B", new ArrayList<>(), Arrays.asList(3, 4, 5), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Parents parents = new Parents(parentA, parentB);
        List<Integer> crossover = onePointCrossover.crossoverDurations(parents);
        for(Integer duration : parentA.getDurations()) {
            Assertions.assertTrue(crossover.contains(duration));
        }
    }

    @Test
    void crossoverDurationParentsSameLength() {
        ParallelisationService.generateIds(2);
        Chromosome parentA = new Chromosome("A", new ArrayList<>(), Arrays.asList(1, 2), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome parentB = new Chromosome("B", new ArrayList<>(), Arrays.asList(3, 4), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Parents parents = new Parents(parentA, parentB);
        List<Integer> crossover = onePointCrossover.crossoverDurations(parents);
        for(Integer duration : parentB.getDurations()) {
            Assertions.assertTrue(crossover.contains(duration));
        }
    }

    @Test
    void crossoverStatesParentALonger() {
        ParallelisationService.generateIds(2);
        Chromosome parentA = new Chromosome("A", Arrays.asList(1, 2, 3), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome parentB = new Chromosome("B", Arrays.asList(4, 5), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Parents parents = new Parents(parentA, parentB);
        List<Integer> crossover = onePointCrossover.crossoverSignals(parents);
        for(Integer signal : parentB.getSignals()) {
            Assertions.assertTrue(crossover.contains(signal));
        }
    }

    @Test
    void crossoverStatesParentBLonger() {
        ParallelisationService.generateIds(2);
        Chromosome parentA = new Chromosome("A", Arrays.asList(1, 2), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome parentB = new Chromosome("B", Arrays.asList(3, 4, 5), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Parents parents = new Parents(parentA, parentB);
        List<Integer> crossover = onePointCrossover.crossoverSignals(parents);
        for(Integer signal : parentA.getSignals()) {
            Assertions.assertTrue(crossover.contains(signal));
        }
    }

    @Test
    void crossoverStatesParentsSameLength() {
        ParallelisationService.generateIds(2);
        Chromosome parentA = new Chromosome("A", Arrays.asList(1, 2), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome parentB = new Chromosome("B", Arrays.asList(3, 4), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Parents parents = new Parents(parentA, parentB);
        List<Integer> crossover = onePointCrossover.crossoverSignals(parents);
        for(Integer signal : parentB.getSignals()) {
            Assertions.assertTrue(crossover.contains(signal));
        }
    }
}