package EA.operators.crossover;

import DTO.Fitness;
import common.data.Chromosome;
import EA.service.CrossoverService;
import common.service.ParallelisationService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class CrossoverServiceTest {

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @Test
    void repairDurationsLongerThanSignals() {
        ParallelisationService.generateIds(1);
        Chromosome child = new Chromosome("A", Arrays.asList(1, 2), Arrays.asList(1, 2, 3), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome repairedChild = CrossoverService.makeDurationsAndSignalsOfSameLength(child);
        Assertions.assertEquals(repairedChild.getSignals(), Arrays.asList(1, 2));
        Assertions.assertEquals(repairedChild.getDurations(), Arrays.asList(1, 2));
    }

    @Test
    void repairSignalsLongerThanDurations() {
        ParallelisationService.generateIds(1);
        Chromosome child = new Chromosome("A", Arrays.asList(1, 2, 3), Arrays.asList(1, 2), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome repairedChild = CrossoverService.makeDurationsAndSignalsOfSameLength(child);
        Assertions.assertEquals(repairedChild.getSignals(), Arrays.asList(1, 2));
        Assertions.assertEquals(repairedChild.getDurations(), Arrays.asList(1, 2));
    }

    @Test
    void repairSignalsAsLongAsDurations() {
        ParallelisationService.generateIds(1);
        Chromosome child = new Chromosome("A", Arrays.asList(1, 2), Arrays.asList(1, 2), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome repairedChild = CrossoverService.makeDurationsAndSignalsOfSameLength(child);
        Assertions.assertEquals(repairedChild, child);
    }


}