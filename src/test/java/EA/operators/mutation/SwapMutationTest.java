package EA.operators.mutation;

import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class SwapMutationTest {

    SwapMutation swapMutation;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp() {
        swapMutation = new SwapMutation();
    }

    @Test
    void checkMutateContainsSameElementsInDifferentOrder() {
        List<Integer> signals = Arrays.asList(1, 2);
        List<Integer> mutatedSignals = swapMutation.mutate(new ArrayList<>(signals));
        Assertions.assertEquals(mutatedSignals.get(0), signals.get(1));
        Assertions.assertEquals(mutatedSignals.get(1), signals.get(0));
    }
}