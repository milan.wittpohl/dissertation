package EA.operators.mutation;

import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class OffspringMutationTest {

    OffspringMutation offspringMutation;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp() {
        offspringMutation = new OffspringMutation();
    }

    @Test
    void checkMutateContainsSameElementsInDifferentOrder() {
        List<Integer> signals = Arrays.asList(1, 2);
        List<Integer> mutatedSignals = offspringMutation.mutate(new ArrayList<>(signals));
        Assertions.assertNotEquals(signals, mutatedSignals);
    }
}