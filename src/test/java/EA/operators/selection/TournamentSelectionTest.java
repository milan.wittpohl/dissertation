package EA.operators.selection;

import DTO.Fitness;
import DTO.Parents;
import common.data.Chromosome;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

class TournamentSelectionTest {

    TournamentSelection tournamentSelection;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp(){
        tournamentSelection = new TournamentSelection();
    }

    @Test
    void selectBestTwoOfFour() {
        ConcurrentLinkedQueue<Chromosome> chromosomes = new ConcurrentLinkedQueue<>();

        Chromosome mockedChromosomeA = new Chromosome("A", new ArrayList<>(),new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome mockedChromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome mockedChromosomeC = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 0);
        Chromosome mockedChromosomeD = new Chromosome("D", new ArrayList<>(), new ArrayList<>(), new Fitness(3.0, 0.0, 0.0, 0.0), 0);

        chromosomes.add(mockedChromosomeA);
        chromosomes.add(mockedChromosomeB);
        chromosomes.add(mockedChromosomeC);
        chromosomes.add(mockedChromosomeD);

        Parents parents = tournamentSelection.select(chromosomes);

        Assertions.assertEquals(parents.getParentA(), mockedChromosomeA);
        Assertions.assertEquals(parents.getParentB(), mockedChromosomeB);
    }
}