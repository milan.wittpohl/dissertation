package EA.operators.selection;

import DTO.Fitness;
import DTO.Parents;
import common.data.Chromosome;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

class RankingSelectionTest {

    RankingSelection rankingSelection;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp(){
        rankingSelection = new RankingSelection();
    }

    @Test
    void selectBestTwoOfFour() {
        ConcurrentLinkedQueue<Chromosome> chromosomes = new ConcurrentLinkedQueue<>();

        Chromosome mockedChromosomeA = new Chromosome("A", new ArrayList<>(),new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome mockedChromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome mockedChromosomeC = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(2000.0, 0.0, 0.0, 0.0), 0);
        Chromosome mockedChromosomeD = new Chromosome("D", new ArrayList<>(), new ArrayList<>(), new Fitness(3000.0, 0.0, 0.0, 0.0), 0);

        chromosomes.add(mockedChromosomeA);
        chromosomes.add(mockedChromosomeB);
        chromosomes.add(mockedChromosomeC);
        chromosomes.add(mockedChromosomeD);

        Parents parents = rankingSelection.select(chromosomes);

        Assertions.assertFalse(parents.getParentA().equals(mockedChromosomeC) && parents.getParentB().equals(mockedChromosomeC));
        Assertions.assertFalse(parents.getParentA().equals(mockedChromosomeD) && parents.getParentB().equals(mockedChromosomeD));
    }
}